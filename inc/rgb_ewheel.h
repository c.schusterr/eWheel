/**
 * @file Wheel_RGB.h
 * @author Kass Chupongstimun
 * @author Michael Siem
 * @date 1/11/17
 * @author Mostafa Hassan (updated 11/6/17)
 *
 * @brief This file provides includes and definitions for Wheel_RGB.c
 *
 * This file is part of a CCS project and intended to be built with CCS
 */
 
#ifndef __Wheel_RGB_H_
#define __Wheel_RGB_H_

//******************************************************************************
//
// Defines
//
//******************************************************************************
#define SIZE        10
#define PI          3.14159265358979323846
#define MAX_SPEED   40

/**
 * @brief RGB Patterns
 */
typedef enum mode {
    STARTUP,        //!< Pattern 1 */
    READY,          //!< Pattern 2 */
    SPEED,          //!< Pattern 3 */
    LOW_BATTERY,    //!< Pattern 5 */
    IDLE            //!< Pattern 6 */
} Mode;

//******************************************************************************
//
// Function Declarations
//
//******************************************************************************
void RGBInit(void);
void RGBUpdate(void);
void RGBStartUpPattern(void);
bool RGBSetSpeedScale(void);
void RGBTurnOn(uint32_t color, uint8_t number);
void RGBTurnOffAll(void);
void RGBPulsate(void);
void RGBMaxPowerPattern(void);
bool RGBReadyPattern(void);
void RGBIdlePattern(void);
void RGBLowBatteryPattern(void);

#endif /* __Wheel_RGB_H_ */
