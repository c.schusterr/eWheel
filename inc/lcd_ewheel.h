/**
 *
 */

#ifndef __Wheel_LCD_H_
#define __Wheel_LCD_H_

#include "grlib/grlib.h" // needed for function declarations
#include "grlib/widget.h" // needed for function declarations
#include "grlib/canvas.h" // needed for function declarations

//*****************************************************************************
//
// Defines
//
//*****************************************************************************
#define MOTOR_RED_TEMP					80
#define BATTERY_PACK_RED_TEMP			60

/**
 * @brief Screens
 */
typedef enum screen {
	DRIVER,			    /**< Main Screen*/
	PARAMETERS, 	    /**< Diagnostics Screen*/
	FAULTS,			    /**< Active Faults Screen*/
	DIAGNOSTICS,        /**< Hardware Diagnostics Screen*/
	COMMUNICATIONS      /**< CAN Communications Screen*/
} Screen;

//*****************************************************************************
//
// Function Declarations
//
//*****************************************************************************
void GraphicsInit(void);

//
// On context paint functions
//
void OnSplashPaint(tWidget *pWidget, tContext *pContext);
void OnDriverPaint(tWidget *pWidget, tContext *pContext);
void OnParametersPaint(tWidget *pWidget, tContext *pContext);
void OnFaultsWarningPaint(tWidget *pWidget, tContext *pContext);
void OnBatteryGraphicPaint(tWidget *pWidget, tContext *pContext);
void OnHardwareDiagnosticsPaint(tWidget *pWidget, tContext *pContext);
void OnComsPaint(tWidget *pWidget, tContext *pContext);

//
// Driver Screen
//
void SpeedPaint(void);
void ChargePaint(void);
void BatteryGraphicPaint(void);
void FaultsWarningPaint(void);
void ECUDriverFlagsPaint(void);

//
// Parameters Screen
//
void PackSummedVoltagePaint(void);
void PackCurrentPaint(void);
void PackHealthPaint(void);
void PackTempPaint(void);
void HighTempPaint(void);
void LVPaint(void);
void HealthPaint(void);
void MotorTemperaturesPaint(void);

//
// Diagnostics Screen
//
void DigitalInputPaint(tCanvasWidget* widget, int value);

//
// LCD Control
//
void LCDChangeScreen(void);
void LCDUpdateScreen(void);
void LCDUpdateDriverScreen(void);
void LCDUpdateParametersScreen(void);
void LCDUpdateDialFlags(uint16_t buttons);
bool LCDUpdateFaultsScreen(void);
void LCDUpdateHardwareDiagnosticsScreen(void);
void LCDUpdateCommunicationsScreen(void);

#endif /* Wheel_LCD_H_ */
