/**
 *
 */

#ifndef __Wheel_CAN_H_
#define __Wheel_CAN_H_

//*****************************************************************************
//
// Defines
//
//*****************************************************************************

//-----------------------------------------------------------------------------
// CAN Signal Scaling Factors
//-----------------------------------------------------------------------------

// BMS Vitals
#define CAN_SIG_CELL_HIGH_TEMP				1
#define CAN_SIG_CELL_AVG_TEMP               1
#define CAN_SIG_CELL_HIGH_VOLT              0.1
#define CAN_SIG_CELL_LOW_VOLT               0.1

// BMS SOC
#define CAN_SIG_SOC_PERCENT                 0.5

// Pack Stats
#define CAN_SIG_PACK_SUMMED_VOLTAGE         0.01
#define CAN_SIG_PACK_CURRENT                0.1

// Aux MCU
#define CAN_SIG_SDC_SUPPLY_V                0.1
#define CAN_SID_SDC_RETURN_V                0.1
#define CAN_SID_LV_DCDC_V                   0.1
#define CAN_SID_FIVE_V                      0.1
#define CAN_SID_BRK_PRES1_V                 0.1
#define CAN_SID_IMD_FAULT_V                 0.1
#define CAN_SID_AIR_COIL_V                  0.1
#define CAN_SID_LV_BATT_V                   0.1

// SBG_ECAN_LOG_EKF_VEL - used for speed
#define CAN_SIG_EKF_VEL_N					0.01
#define CAN_SIG_EKF_VEL_E					0.01

// Motor Controller
#define CAN_SIG_CONTROLLER_PHASE_CURRENT	0.01
#define CAN_SIG_CONTROLLER_MOTOR_SPEED		1
#define CAN_SIG_CONTROLLER_BUS_VOLTAGE		0.1
#define CAN_SIG_CONTROLLER_MOTOR_TEMP		1
#define CAN_SIG_CONTROLLER_TEMP				1

// DAQ
#define CAN_SIG_DAQ_29_LV_INT			    0.1

//-----------------------------------------------------------------------------
// CAN Identifiers and Definitions
//-----------------------------------------------------------------------------

//
// Button States
//
#define CAN_BUTTONS_MSG_ID					0xD5
#define CAN_BUTTONS_LENGTH					2
#define CAN_TX_BUTTONS_OBJ_ID				1

//
// [ECU] MOTOR FAULTS
//
#define CAN_RX_ECU_MOTOR_FLAGS_MSG_ID		0xF1
#define CAN_RX_ECU_MOTOR_FLAGS_LENGTH		8
#define CAN_RX_ECU_MOTOR_FLAGS_OBJ_ID		3

//
// [ECU] OTHER FAULTS
//
#define CAN_RX_ECU_OTHER_FLAGS_MSG_ID		0xF2
#define CAN_RX_ECU_OTHER_FLAGS_LENGTH		8
#define CAN_RX_ECU_OTHER_FLAGS_OBJ_ID		4

//
// [ECU] PERFORMANCE FLAGS
//
#define CAN_RX_ECU_PERFORMANCE_FLAGS_MSG_ID		0xF3
#define CAN_RX_ECU_PERFORMANCE_FLAGS_LENGTH		8
#define CAN_RX_ECU_PERFORMANCE_FLAGS_OBJ_ID		5

//
// BMS Vitals
//
#define CAN_RX_BMS_VITALS_MSG_ID			0x40
#define	CAN_RX_BMS_VITALS_LENGTH			8
#define CAN_RX_BMS_VITALS_OBJ_ID			10

//
// BMS SOC Message
//
#define CAN_RX_BMS_SOC_MSG_ID               0x43
#define CAN_RX_BMS_SOC_LENGTH               8
#define CAN_RX_BMS_SOC_OBJ_ID               11

//
// BMS Pack Stats
//
#define CAN_RX_BMS_PACK_STATS_MSG_ID	    0x41
#define CAN_RX_BMS_PACK_STATS_LENGTH		8
#define CAN_RX_BMS_PACK_STATS_OBJ_ID		12

//
// Aux MCU Digital
//
#define CAN_RX_AUX_DIG_MSG_ID    0x30
#define CAN_RX_AUX_DIG_LENGTH    2
#define CAN_RX_AUX_DIG_OBJ_ID    16

//
// Aux MCU Analog
//
#define CAN_RX_AUX_ANALOG_MSG_ID    0x130
#define CAN_RX_AUX_ANALOG_LENGTH    8
#define CAN_RX_AUX_ANALOG_OBJ_ID    17

//
// FR Motor Controller Status
//
#define CAN_RX_CONTROLLER_FL_MSG_ID         0xC0
#define CAN_RX_CONTROLLER_FL_LENGTH         8
#define CAN_RX_CONTROLLER_FL_OBJ_ID         22

//
// FL Motor Controller Status
//
#define CAN_RX_CONTROLLER_FR_MSG_ID			0xC4
#define CAN_RX_CONTROLLER_FR_LENGTH			8
#define CAN_RX_CONTROLLER_FR_OBJ_ID			21

//
// RL Motor Controller Status
//
#define CAN_RX_CONTROLLER_RL_MSG_ID			0xC8
#define CAN_RX_CONTROLLER_RL_LENGTH			8
#define CAN_RX_CONTROLLER_RL_OBJ_ID			23

//
// RR Motor Controller Status
//
#define CAN_RX_CONTROLLER_RR_MSG_ID			0xCC
#define CAN_RX_CONTROLLER_RR_LENGTH			8
#define CAN_RX_CONTROLLER_RR_OBJ_ID			24

//
// Bosch Message 1
//
#define CAN_RX_BOSCH_YRS_1_MSG_ID           0x70
#define CAN_RX_BOSCH_YRS_1_LENGTH           8
#define CAN_RX_BOSCH_YRS_1_OBJ_ID           25


//
// Speed - EKF VEL
//
#define CAN_RX_EKF_VE_MSGL_ID				0x137
#define CAN_RX_EKF_VEL_LENGTH				7
#define CAN_RX_EKF_VEL_OBJ_ID				29

//
// Steering Angle Sensor - SDO Response
//
#define CAN_RX_SDO_RESP_MSG_ID               0x5FF
#define CAN_RX_SDO_RESP_LENGTH               8
#define CAN_RX_SDO_RESP_OBJ_ID               30

//
// DAQ 13
//
#define CAN_RX_DAQ_13_MSG_ID				0x313
#define CAN_RX_DAQ_13_LENGTH				8
#define CAN_RX_DAQ_13_OBJ_ID				31

//
// DAQ 29
//
#define CAN_RX_DAQ_29_MSG_ID				0x329
#define CAN_RX_DAQ_29_LENGTH				6
#define CAN_RX_DAQ_29_OBJ_ID				32

//-----------------------------------------------------------------------------
// ECU Flags Bit Position macros
//-----------------------------------------------------------------------------
#define ECU_FLAG_REGEN_EN					1 << 3
#define ECU_FLAG_READY_TO_DRIVE				1 << 6

//*****************************************************************************
//
// Typedefs and Structs
//
//*****************************************************************************

struct BMS_VITALS
{
    int8_t   highTemp;                  //!<
    uint8_t  avgTemp;
    uint8_t  highCellVolt;
    uint8_t  lowCellVolt;
};

struct BMS_SOC
{
    uint8_t soc;
};

struct BMS_PACK
{
    uint16_t packSummedVoltage;
    int16_t packCurrent;
};

struct AUX_DIG
{
    bool airs_closed;
    bool ecu_fault;
    bool sdc_supply;
    bool sdc_return;
    bool imd_fault;
    bool bpd_fault;
    bool mux_batt_valid;
    bool mux_dcdc_valid;
    bool bms_fault;
};

struct AUX_ANALOG
{
    float sdc_supply_v;
    float sdc_return_v;
    float lv_dcdc_v;
    float five_v;
    float brk_pres1_v;
    float imd_fault_v;
    float air_coil_v;
    float lv_batt_v;
};

struct ECU_FLAGS
{
    uint32_t ECUMotorFlags1;
    uint32_t ECUMotorFlags2;
    uint32_t ECUOtherFlags;
    uint32_t ECUPerformanceFlags;
};

struct EKF_VEL
{
    int16_t velocity_N;
    int16_t velocity_E;
};

struct DAQ
{
    uint8_t torqueVectoring;
    uint8_t lvInt;
    uint8_t lvDec;
};

struct MOTOR_TEMPS
{
    uint8_t motorTempFL;
    uint8_t motorTempFR;
    uint8_t motorTempRL;
    uint8_t motorTempRR;
};

/**
 * @brief Stores all of the parameters that are received over CAN
 */
typedef struct
{
    struct BMS_VITALS bmsVitals;
    struct BMS_SOC bmsSOC;
    struct BMS_PACK bmsPackStats;
    struct AUX_DIG auxDig;
    struct AUX_ANALOG auxAnalog;
    struct ECU_FLAGS ecuFlags;
    struct EKF_VEL ekfVel;
    struct DAQ daq;
    struct MOTOR_TEMPS motorTemps;
} CAN_Params;

CAN_Params CANParams;

//*****************************************************************************
//
// Function Declarations
//
//*****************************************************************************
void CANDataHandler(void);
void CANInitialize(void);
void CANTxButtons(uint32_t buttons, uint16_t analog0, uint16_t analog1);

bool noFaults(void);
uint16_t GetSpeed(void);

#endif /* Wheel_Buttons_H_ */
