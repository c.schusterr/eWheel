/**
 * @brief watchdog
 *
 */

#ifndef _WHEEL_WATCHDOG_H_
#define _WHEEL_WATCHDOG_H_

//******************************************************************************
//
// Function Declarations
//
//******************************************************************************
void WatchdogInitialize(void);

#endif /* _WHEEL_WATCHDOG_H_ */
