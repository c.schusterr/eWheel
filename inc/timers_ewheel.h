/**
 * @brief This file provides defines, includes, and function declarations for
 * Wheel_Timers.c
 *
 */

#ifndef __Wheel_Timers_H_
#define __Wheel_Timers_H_

//*****************************************************************************
//
// Defines
//
//*****************************************************************************

#define FLAG_1MS	0b0000001	//!< bitmask for 1MS timer, set bit 0
#define FLAG_10MS	0b0000010	//!< bitmask for 10MS timer, set bit 1
#define FLAG_100MS	0b0000100 	//!< bitmask for 100MS timer, set bit 2
#define	FLAG_250MS	0b0001000	//!< bitmask for 250MS timer, set bit 3
#define	FLAG_1000MS	0b0010000	//!< bitmask for 1000MS timer, set bit 4
#define TIMER_50ms  4000000
#define TIMER_MAX   0xffffffff

#define TIMER_TICKS_MAX     0xffffffff
#define TIMER_TICKS_1MS     80000
#define TIMER_TICKS_5MS     400000
#define TIMER_TICKS_10MS    800000
#define TIMER_TICKS_25MS    2000000
#define TIMER_TICKS_50MS    4000000
#define TIMER_TICKS_100MS   8000000
#define TIMER_TICKS_1S      80000000
#define TIMER_TICKS_2S      160000000
#define TIMER_TICKS_10S     800000000

//*****************************************************************************
//
// Global Variables
//
//*****************************************************************************
extern volatile uint8_t timerFlags;

//*****************************************************************************
//
// Void Function Declarations
//
//*****************************************************************************
void Timer0AInit(void);
void Timer0IntHandler(void);
uint8_t GetTimerFlags(void);
void timer2A_init(void);
uint32_t get_time(void);
bool timeout_occured(uint32_t *time, uint32_t timeout);


#endif /* Wheel_Timers_H_ */
