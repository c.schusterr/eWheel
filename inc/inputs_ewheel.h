/**
 *
 */

#ifndef __Wheel_Buttons_H_
#define __Wheel_Buttons_H_

#include <stdbool.h>
#include <stdint.h>

//*****************************************************************************
//
// Defines
//
//*****************************************************************************
#define NUM_INPUTS					22
#define MAX_INPUT_CHECKS 			10 	//!< required number checks for a button to be pressed

// Define the bit position of each button for TX CAN packet to work with the ECU
// Bit position macros are also used on the steering wheel to see if a button is
// pressed for example changing screens.
#define INPUT_PA6      					0
#define INPUT_PA7				        1
#define INPUT_PB2			        	2
#define INPUT_PE1  	                	3
#define INPUT_PB0                  		4
#define INPUT_PF1              			5
#define INPUT_PE3              			6
#define INPUT_PA3	                    7

#define INPUT_PE2          				8
#define INPUT_PE5          				9
#define INPUT_PE4         				10
#define INPUT_PB1         				11
#define INPUT_PD5                       12
#define INPUT_PA0                       13
#define INPUT_PA1                       14
#define INPUT_PB3                       15
#define INPUT_PD2                       16
#define INPUT_PD4                       17
#define INPUT_PD6                       18
#define INPUT_PB7                       19
#define INPUT_PB6                       20
#define INPUT_PB5                       21

#define INPUT_TC_MIN                    INPUT_PA6
#define INPUT_TC_MID                    INPUT_PA7
#define INPUT_TC_MAX                    INPUT_PB2
#define BUTTON_4                        INPUT_PE1
#define BUTTONS_AUTOCROSS_MODE          INPUT_PB0
#define BUTTONS_ENDURANCE_MODE          INPUT_PF1
#define BUTTONS_SKIDPAD_MODE            INPUT_PE3
#define BUTTONS_ACCELERATION_MODE       INPUT_PA3
#define BUTTONS_BUTTON_1                INPUT_PE2
#define BUTTONS_BUTTON_2                INPUT_PE5
#define BUTTONS_BUTTON_3                INPUT_PE4
#define BUTTONS_BUTTON_4                INPUT_PB1
#define BUTTONS_BUTTON_5                INPUT_PD5
#define BUTTONS_BUTTON_6                INPUT_PA0
#define BUTTONS_BUTTON_7                INPUT_PA1
#define BUTTONS_BUTTON_8                INPUT_PB3
#define BUTTONS_BUTTON_9                INPUT_PB5
#define BUTTONS_BUTTON_10               INPUT_PB6
#define BUTTONS_BUTTON_11               INPUT_PB7
#define BUTTONS_BUTTON_12               INPUT_PD6
#define BUTTONS_BUTTON_13               INPUT_PD4
#define BUTTONS_BUTTON_14               INPUT_PD2

//
// buttons masks - used to OR with the buttons variable to determine if a button
// is pressed
//
#define BUTTONS_BUTTON_1_M				(1 << BUTTONS_BUTTON_1)
#define BUTTONS_BUTTON_2_M				(1 << BUTTONS_BUTTON_2)
#define BUTTONS_BUTTON_3_M				(1 << BUTTONS_BUTTON_3)
#define BUTTONS_BUTTON_4_M				(1 << BUTTONS_BUTTON_4)

#define BUTTONS_TC_MIN_M				(1 << INPUT_TC_MIN)
#define BUTTONS_TC_MID_M				(1 << INPUT_TC_MID)
#define BUTTONS_TC_MAX_M				(1 << INPUT_TC_MAX)
#define BUTTON_4_M		                (1 << BUTTON_4)
#define BUTTONS_AUTOCROSS_MODE_M		(1 << BUTTONS_AUTOCROSS_MODE)
#define BUTTONS_ENDURANCE_MODE_M		(1 << BUTTONS_ENDURANCE_MODE)
#define BUTTONS_SKIDPAD_MODE_M			(1 << BUTTONS_SKIDPAD_MODE)
#define BUTTONS_ACCELERATION_MODE_M		(1 << BUTTONS_ACCELERATION_MODE)

#define CHANGE_SCREENS_M				BUTTONS_BUTTON_3_M
#define BUTTON_SW2                      12

//*****************************************************************************
//
// Function Declarations
//
//*****************************************************************************
void DigitalInputInit(void);
void AnalogInputInit(void);
void AnalogInputRead();
void InputDebounce(void);
void AnalogInputRead(uint16_t * value0, uint16_t * value1);
uint32_t GetButtons(void);
uint16_t GetAnalog0(void);
uint16_t GetAnalog1(void);

#endif /* Wheel_Buttons_H_ */
