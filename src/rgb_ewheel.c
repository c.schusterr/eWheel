/**
 * @file Wheel_RGB.c
 * @author Kass Chupongstimun
 * @author Michael Siem
 * @date 1/11/17
 * @author Mostafa Hassan (updated 11/6/17)
 *
 * @brief interface for RGB bar
 *
 * This file is part of a CCS project and intended to be built with CCS
 */

//******************************************************************************
//
// Includes
//
//******************************************************************************
#include <stdint.h>
#include <stdbool.h>
#include "stdlib.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_uart.h"
#include "inc/hw_gpio.h"
#include "inc/hw_ssi.h"
#include "inc/hw_pwm.h"
#include "inc/hw_types.h"
#include "driverlib/timer.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/udma.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"
#include "driverlib/systick.h"
#include <string.h>
#include "can_ewheel.h"
#include "math.h"
#include "rgb_ewheel.h"

//******************************************************************************
//
// Global variables for this file
//
//******************************************************************************
static Mode currMode = STARTUP;     //!< Current RGB Pattern Mode
uint32_t RGB[SIZE];                 //!< RGB color values

//
// Colors are in the format G  R  B
//                       0x GG RR BB

/**
 * @brief Hardware initialization for RGB Bar
 *
 * Uses SPI peripheral to send data serially to the WS2812b RGB LEDs
 */
void RGBInit(void)
{
    // enable clocking and wait for it to be ready
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_SSI1)) {};

    // configure PD3 (RGB Data)
    GPIOPinConfigure(GPIO_PD3_SSI1TX);
    GPIOPinTypeSSI(GPIO_PORTD_BASE, GPIO_PIN_3);

    //SSIIntClear(SSI1_BASE,SSI_TXEOT);
    SSIConfigSetExpClk(SSI1_BASE, 80000000, SSI_FRF_MOTO_MODE_0,SSI_MODE_MASTER, 6400000, 8);
    SSIEnable(SSI1_BASE);
}

/**
 * @brief send pattern to RGB bar
 *
 * @param values specifies the colors for each LED
 * @param number of LEDs
 *
 * Translate the array of uint32_t into the waveform required by the WS2812b
 * RGB LEDs
 */
void SendData(uint32_t values[],uint8_t number)
{
    int j, i;
    for (j = 0; j < number; j++)
    {
        for (i = 23; i >= 0; i--)
        {
            // Waveform conversion
            volatile uint8_t convert = 0xC0 ;   //0b11000000;

            if ( (values[j] >> i) & 0x1 != 0)
            {
                convert = 0xF8;                 // 0b11111000;
            }

            // Send the processed data
            SSIDataPut(SSI1_BASE, convert);
        }
    }
}

/**
 * @brief Compile RGB values into 1 uint32_t
 *
 * @author Kass Chupongstimun
 * @date 5/1/17
 *
 * @param R value for Red
 * @param G value for Green
 * @param B value for Blue
 * @return Combination of Red, Green, and Blue values
 */
uint32_t ConvertRGB(uint8_t R, uint8_t G, uint8_t B)
{
    return ( (G << 16) + (R << 8) + B );
}

/**
 * @brief update RGB bar
 * @author Kass Chupongstimun
 * @date 5/1/17
 *
 * Updates the pattern of the RGB bar
 */
void RGBUpdate(void)
{
    //
    // STARTUP Mode, pulsate until Ready to Drive flag received
    //
    if (currMode == STARTUP)
    {
        RGBPulsate();
        uint32_t flags = CANParams.ecuFlags.ECUPerformanceFlags;

        if (flags & ECU_FLAG_READY_TO_DRIVE)
        {
            currMode = READY;
        }
    }

    //
    // Flash the "Loading Bar" pattern, then go to SPEED Mode
    //
    else if (currMode == READY)
    {
        bool done = RGBReadyPattern();

        if (done)
        {
            currMode = SPEED;
        }
    }

    //
    // Sets RGB pattern to indicate speed
    //
    else if (currMode == SPEED)
    {
        //
        // If charge less than 10 percent flash RED
        //
        if (CANParams.bmsSOC.soc <= 10)
        {
            currMode = LOW_BATTERY;
        }
        //If speed <= 4 mph for 10 seconds, switch to IDLE mode
        else if(RGBSetSpeedScale())
        {
            currMode = IDLE;
        }
    }

    //
    // LOW_BATTERY
    //
    else if (currMode == LOW_BATTERY)
    {
        RGBLowBatteryPattern();
        if(CANParams.bmsSOC.soc > 10) currMode = SPEED;
    }

    //
    // IDLE
    //
    else if (currMode == IDLE)
    {
        if(!RGBSetSpeedScale()) RGBIdlePattern();
        else currMode = SPEED;
    }
}

/**
 * @brief scales RGB bar to car speed
 *
 * @author Mostafa Hassan
 * @date 11/6/17
 *
 */
bool RGBSetSpeedScale(void)
{
    static uint8_t speedScaleOld = 0;
    static float tick = 0.00; //switches to IDLE mode after 10 seconds of low speed

    uint16_t speed = GetSpeed();

    uint8_t speedScale;

    if (speed < MAX_SPEED/20)
    {
        speedScale = 0;
        tick += 0.01; //every 10 ms
    }
    else if (speed < MAX_SPEED/10)
    {
        speedScale = 1;
        tick += 0.01; //every 10 ms
    }
    else if (speed < 2*MAX_SPEED/10)
    {
        speedScale = 2;
        tick = 0.00;
    }
    else if (speed < 3*MAX_SPEED/10)
    {
        speedScale = 3;
        tick = 0.00;
    }
    else if (speed < 4*MAX_SPEED/10)
    {
        speedScale = 4;
        tick = 0.00;
    }
    else if (speed < 5*MAX_SPEED/10)
    {
        speedScale = 5;
        tick = 0.00;
    }
    else if (speed < 6*MAX_SPEED/10)
    {
        speedScale = 6;
        tick = 0.00;
    }
    else if (speed < 7*MAX_SPEED/10)
    {
        speedScale = 7;
        tick = 0.00;
    }
    else if (speed < 8*MAX_SPEED/10)
    {
        speedScale = 8;
        tick = 0.00;
    }
    else if (speed < 9*MAX_SPEED/10)
    {
        speedScale = 9;
        tick = 0.00;
    }
    else
    {
        speedScale = 10;
        tick = 0.00;
    }

    // Turn on LEDs according to speed
    if (speedScale != speedScaleOld)
    {
        RGBTurnOn(ConvertRGB(0, 0, speedScale), speedScale);
        speedScaleOld = speedScale;
    }

    return (tick >= 10.00);
}

/**
 * @brief RGB Pulsates 
 *
 * @author Kass Chupongstimun
 * @date 5/1/17
 */
void RGBPulsate(void)
{
    // 10 ms per TICK
    const float TICK = 0.01;

    // Hz Frequency for pulsating waveform
    const float FREQ_DIM = 0.32;

    // t keeps track of time
    static float t = 0.0;

    // Stores the brightness level of each LED
    float brightness;

    // Progress time
    t += TICK;

    uint8_t R, G, B;

    brightness = sin(2*PI*FREQ_DIM*t) + 1.00;

    R = (uint8_t)(0.0 * brightness);
    G = (uint8_t)(17.0 * brightness);
    B = (uint8_t)(4.0 * brightness);

    int i;
    for (i = 0; i < SIZE; i++)
    {
        RGB[i] = ConvertRGB(R, G, B);
    }

    SendData(RGB, SIZE);
}

/**
 * @brief RGB Loading Bar
 *
 * @author Kass Chupongstimun
 * @date 5/1/17
 */
bool RGBReadyPattern(void)
{
    static uint16_t counter = 0;
    static uint16_t num = 0;

    counter++;

    if (counter > 8)
    {
        counter = 0;
        num++;
    }

    RGBTurnOn(ConvertRGB(0, 17, 4), num);

    if (num > 11)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * @author Kass Chupongstimun
 * @date 5/1/17
 *
 * Turn on @p number LEDS to be @p color
 */
void RGBTurnOn(uint32_t color, uint8_t number)
{
    int i;
    for (i = 0; i < number; i++)
    {
        RGB[i] = color;
    }
    for (i = number; i < SIZE; i++)
    {
        RGB[i] = 0x0;
    }
    SendData(RGB, SIZE);
}

/**
 * @brief Turn all LEDs off
 *
 * @author Kass Chupongstimun
 * @date 5/1/17
 */
void RGBTurnOffAll()
{
    int i;
    for(i = 0; i < SIZE; i++)
    {
        RGB[i] = 0x0;
    }
    SendData(RGB, SIZE);
}

/**
 * @brief Low Battery RGB Pattern
 *
 * @author Michael Siem
 *
 * Flash Red at 1Hz when the battery charge is less than 10 percent
 */
void RGBLowBatteryPattern(void)
{
    static uint8_t count = 0;

    count++;

    if (count == 100)
    {
        RGBTurnOn(0x00FF00, SIZE);
    }

    else if (count == 200)
    {
        RGBTurnOffAll();
        count = 0;
    }
}

/**
 * @brief RGB IDLE pattern
 *
 * @author Mostafa Hassan
 * @date 3/27/18
 *
 * All green if no faults, all red if faulted.
 */
void RGBIdlePattern(void)
{
    bool faulted = !noFaults();

    if(faulted)
    {
        RGBTurnOn(0x00FF00, SIZE);
    }else
    {
        RGBTurnOn(0xFF0000, SIZE);
    }

}

/**
 * @brief RGB start up pattern
 *
 * @author Kass Chupongstimun
 * @date 5/1/17
 */
void RGBStartUpPattern(void)
{
    // 10 ms per TICK
    const float TICK = 0.01;

    // Hz Frequency for color waveform
    const float FREQ_COLOR = 1.5;

    // Hz Frequency for dimming waveform
    const float FREQ_DIM = 1.75;

    // Radian Phase difference between adjacent LEDs
    const float PHASE_DIFF = (2.0*PI) / 10;

    // t keeps track of time
    static float t = 0.0;

    // Stores the brightness level of each LED
    float brightness[SIZE];

    // Progress time
    t += TICK;

    float red = 0.0;
    float green = sin(2*PI*FREQ_COLOR*t + 0.0) + 1.95;
    float blue = sin(2*PI*FREQ_COLOR*t + 0.65*PI) + 1.95;

    //float red = 4 * TriangleWave(t*FREQ_COLOR + 0.0);
    //float green = 4 * TriangleWave(t*FREQ_COLOR + 0.35);
    //float blue = 4 * TriangleWave(t*FREQ_COLOR + 0.7);

    uint8_t R, G, B;
    int i;
    for (i = 0; i < SIZE; i++)
    {
        brightness[i] = 10.0 * (sin(2*PI*FREQ_DIM*t + i*PHASE_DIFF) + 0.4);
        if (brightness[i] < 1.0)
        {
            brightness[i] = 1.0;
        }
        R = (uint8_t)(red * brightness[i]);
        G = (uint8_t)(green * brightness[i]);
        B = (uint8_t)(blue * brightness[i]);

        RGB[i] = ConvertRGB(R, G, B);
    }

    SendData(RGB, SIZE);
}
