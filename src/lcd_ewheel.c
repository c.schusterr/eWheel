/**
 * @brief LCD Drawing and initialization
 *
 * Functions responsible for configuring the interface with the LCD
 * controller, drawing images on the screen, and updating them.
 *
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "Fonts/fonts.h"
#include "driverlib/sysctl.h"
#include "grlib/grlib.h"
#include "grlib/widget.h"
#include "grlib/canvas.h"
#include "grlib/slider.h"
#include "utils/ustdlib.h"
#include "Lib_images.h"
#include "Lib_draw.h"
#include "Kentec320x240x16_SSD2119_SPI.h"
#include "inputs_ewheel.h"
#include "can_ewheel.h"
#include "lcd_ewheel.h"
#include "timers_ewheel.h"

extern bool BMS;
extern bool DAQ;
extern bool ECU;
extern bool SBG;
extern bool SAS;
extern bool YRS;

//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, unsigned long ulLine)
{
}
#endif

//*****************************************************************************
//
// Constants
//
//*****************************************************************************

//!< Strings for faults messages
const char * const faults_msg[3][40] =
{
	//
	// ECU MOTOR fault messages
	//
	{
	 "FR Motor Low Implausible Temp",         // 0
	 "FL Motor Law Implausible Temp",         // 1
	 "RL Motor Law Implausible Temp",         // 2
	 "RR Motor Law Implausible Temp",         // 3

	 "FR Contr Low Implausible Temp",         // 4
	 "FL Contr Law Implausible Temp",         // 5
	 "RL Contr Law Implausible Temp",         // 6
	 "RR Contr Law Implausible Temp",         // 7

	 "FR Motor High Temp",                    // 8
	 "FL Motor High Temp",                    // 9
	 "RL Motor High Temp",                    // 10
	 "RR Motor High Temp",                    // 11

	 "FR Controller High Temp",               // 12
	 "FL Controller High Temp",               // 13
	 "RL Controller High Temp",               // 14
	 "RR Controller High Temp",               // 15

	 "FR Motor Loss of Serial",               // 16
	 "FL Motor Loss of Serial",               // 17
	 "RL Motor Loss of Serial",               // 18
	 "RR Motor Loss of Serial",               // 19

	 "FR Motor CAN Timeout",                  // 20
	 "FL Motor CAN Timeout",                  // 21
	 "RL Motor CAN Timeout",                  // 22
	 "RR Motor CAN Timeout",                  // 23

	 "FR Motor Implausible Input",            // 24
	 "FL Motor Implausible Input",            // 25
	 "RL Motor Implausible Input",            // 26
	 "RR Motor Implausible Input",            // 27

	 "FR Contr Shorted Disconnected",         // 28
	 "FL Contr Shorted Disconnected",         // 29
	 "RL Contr Shorted Disconnected",         // 30
	 "RR Contr Shorted Disconnected",         // 31

	 "FR Contr DC Current Implausible",       // 32
	 "FL Contr DC Current Implausible",       // 33
	 "RL Contr DC Current Implausible",       // 34
	 "RR Contr DC Current Implausible",       // 35

	 "FR Motor Back Emf Too High",            // 36
	 "FL Motor Back Emf Too High",            // 37
	 "RL Motor Back Emf Too High",            // 38
	 "RR Motor Back Emf Too High"             // 39
	},

	//
	// ECU OTHERS fault messages
	//
	{
		"APPS Short or Disconnect",		      // 0
		"APPS Implausibility",                // 1
		"BSE Short or Disconnect",            // 2
		"BSE Implausibility",                 // 3
		"Steering Sensor Timeout",            // 4
		"Steering Sensor Implausibility",     // 5
		"Ellipse Timeout",                    // 6
		"Ellipse Error Msg Received",         // 7
		"YRS 3 Timout",                       // 8
		"Accum Current Implausibility",       // 9
		"Fans Implausible to Input",          // 10
		"Pumps Implausible to Input",         // 11
		"F anti roll shorted disconnected",   // 12
		"R anti roll shorted disconnected",   // 13
		"BMS CAN Timeout",                    // 14
		"Cell Over Temperature Limit",        // 15
		"Cell Overvoltage Detected",          // 16
		"Cell Undervoltage Detected",         // 17
		"Accum Overvoltage Detected",         // 18
		"Accum Undervoltage Detected",        // 19
		"Cell Temperature Implausiblity",     // 20
		"Cell Voltage Implausibility",        //21
		"Accum Voltage Implausibility",       // 22
		"Accum Current High",                 // 23
		"Accum Current Low",                  // 24
		"Accum Current Implausibility",       // 25
		"Low Voltage System Undervoltage",    // 26
		"Low Voltage System Overvoltage",     // 27
	},

//	//
//	// BMS fault messages
//	//
//	{
//		"Voltage Redundancy F",		// 0
//		"Pack Voltge Sensor F",		// 1
//		"Current Sensor F",			// 2
//		"Open Cell Voltage F",		// 3
//		"Low Cell Voltage F",		// 4
//		"Weak Cell F",				// 5
//		"Internal Conv F",			// 6
//		"Internal Comm F",			// 7
//
//		"Charge-EN Relay F",		// 8
//		"12V Power Supply F",		// 9
//		"HIGH VOLTAGE ISOLATION F",	// 10
//		"Always-On Supply F",		// 11
//		"Communication F",			// 12
//		"Thermistor F",				// 13
//		"Multi-Purpose Output",		// 14
//		"Weak Pack F",				// 15
//
//		"Relay Failsafe EN",		// 16
//		"12V Failsafe EN",			// 17
//		"Current Failsafe EN",		// 18
//		"Voltage Failsafe EN",		// 19
//		"Internal Logic F",			// 20
//		"Internal Thermistor F",	// 21
//		"Internal Memory F",		// 22
//		"Discharge-EN Relay F",		// 23
//
//		"--",	// 24
//		"--",	// 25
//		"--",	// 26
//		"Balancing EN",				// 27
//		"Charge Interlock",			// 28
//		"--",	// 29
//		"--",	// 30
//		"Charger Safety Relay F",	// 31
//
//		"Discharge Relay EN",		// 32
//		"Charger Relay EN",			// 33
//		"Charger Safety EN",		// 34
//		"Malfunction Indicator EN",	// 35
//		"Multi-Purpose Sig"			// 36
//		"Always-On BMS",			// 37
//		"Is-Ready",					// 38
//		"Is-Charging",				// 39
//	}
};

//*****************************************************************************
//
// Global variables for this file
//
//*****************************************************************************

//!< Current screen displayed. Used for changing screen and updating it.
static Screen curr = DRIVER;

//-----------------------------------------------------------------------------
// Strings to hold text for the widgets
//-----------------------------------------------------------------------------
static char pcSpeedText[10];
static char pcHighTempText[10];
static char pcPackCurrentText[10];
static char pcPackSummedVoltageText[10];
static char pcPackStateOfChargeText[10];
static char pcPackTempText[10];
static char pcLVText[10];
static char pcHealthText[10];

// Motor Temperatures
static char pcMotorTempFLText[10];
static char pcMotorTempFRText[10];
static char pcMotorTempRLText[10];
static char pcMotorTempRRText[10];

//Hardware Inputs
static char pcAnalog0Text[10];
static char pcAnalog1Text[10];
static char pcAnalog0VoltageText[10];
static char pcAnalog1VoltageText[10];

//-----------------------------------------------------------------------------
// Forward declarations for parent-child relations to work in the widgets
//-----------------------------------------------------------------------------
extern tCanvasWidget g_sPackStateOfCharge;
extern tCanvasWidget g_sHighTemp;
extern tCanvasWidget g_psFaults[];
extern tCanvasWidget g_sADC1_Voltage;
extern tCanvasWidget g_coms_header;

tContext sContext;

//-----------------------------------------------------------------------------
// First Panel: Wisconsin Racing Splash Screen
//-----------------------------------------------------------------------------

//
// This defines the canvas to be the whole screen. OnSplashPaint actually paints
// the images on the screen within this widget
//
Canvas(g_splashScreen, 0, 0, 0, &g_sKentec320x240x16_SSD2119,
		0, 0, 320, 240,
		CANVAS_STYLE_APP_DRAWN,
		0, 0, 0, 0, 0, 0,
		OnSplashPaint);

//-----------------------------------------------------------------------------
// Second Panel: Driver Display
//-----------------------------------------------------------------------------
//
// this defines the background widget for the driver screen
//
Canvas(g_Driver, 0, 0, &g_sPackStateOfCharge, &g_sKentec320x240x16_SSD2119,
        0, 0, 320, 240,
        CANVAS_STYLE_APP_DRAWN | CANVAS_STYLE_FILL,
        ClrBlack, 0, 0, 0, 0, 0,
        OnDriverPaint);
//
// This defines the widget that updates the BATTERY GRAPHIC
//
Canvas(g_sBatteryGraphic, &g_Driver, 0, 0, &g_sKentec320x240x16_SSD2119,
        5, 5, 310, 40,
        CANVAS_STYLE_APP_DRAWN,
        0, 0, 0, 0, 0, 0,
        OnBatteryGraphicPaint);

//
// This defines the widget that updates the WARNING TRIANGLE GRAPHIC
//
Canvas(g_sFaultsWarning, &g_Driver, &g_sBatteryGraphic, 0, &g_sKentec320x240x16_SSD2119,
        5, 190, 155, 45,
        CANVAS_STYLE_APP_DRAWN,
        0, 0, 0, 0, 0, 0,
        OnFaultsWarningPaint);

//
// This defines the widget that updates the READY TO DRIVE status
//
Canvas(g_sReady, &g_Driver, &g_sFaultsWarning, 0, &g_sKentec320x240x16_SSD2119,
        210, 150, 105, 25,
        CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
        ClrCrimson, 0, ClrOldLace, &g_sFontApercu22b, "RTD", 0, 0);

//
// This defines the widget that updates the TORQUE VECTORING status
//
Canvas(g_sTorqueVectoring, &g_Driver, &g_sReady, 0, &g_sKentec320x240x16_SSD2119,
        210, 180, 105, 25,
        CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
        ClrCrimson, 0, ClrOldLace, &g_sFontApercu22b, "TV", 0, 0);

//
// This defines the widget that updates the TRACTION CONTROL status
//
Canvas(g_sTractionControl, &g_Driver, &g_sTorqueVectoring, 0, &g_sKentec320x240x16_SSD2119,
        210, 210, 105, 25,
        CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
        ClrCrimson, 0, ClrOldLace, &g_sFontApercu22b, "TC", 0, 0);

//
// This defines the widget that updates the DRIVE MODE status
//
Canvas(g_sMode, &g_Driver, &g_sTractionControl, 0, &g_sKentec320x240x16_SSD2119,
        5, 150, 155, 30,
        CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
        ClrCrimson, 0, ClrOldLace, &g_sFontApercu22b, "MODE", 0, 0);

//
// this defines the widget that updates the SPEED text status
//
Canvas(g_sSpeed, &g_Driver, &g_sMode, 0, &g_sKentec320x240x16_SSD2119,
        185, 75, 80, 55,
        CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT | CANVAS_STYLE_TEXT_RIGHT,
        ClrBlack, ClrGray, ClrOldLace, &g_sFontApercu65b, "--", 0, 0);

//
// this defines the widget that updates the PACK STATE OF CHARGE text status
//
Canvas(g_sPackStateOfCharge, &g_Driver, &g_sSpeed, 0, &g_sKentec320x240x16_SSD2119,
        15, 75, 105, 55,
        CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT | CANVAS_STYLE_TEXT_RIGHT,
        ClrBlack, ClrGray, ClrMediumSeaGreen, &g_sFontApercu65b, "--", 0, 0);

//-----------------------------------------------------------------------------
// Third Panel: Parameters Screen
//-----------------------------------------------------------------------------
Canvas(g_Parameters, 0, 0, &g_sHighTemp, &g_sKentec320x240x16_SSD2119,
		0, 0, 320, 240,
		CANVAS_STYLE_APP_DRAWN | CANVAS_STYLE_FILL,
		ClrBlack, 0, 0, 0, 0, 0,
		OnParametersPaint);

//
// this defines the widget that updates the MotorTempRL
//
Canvas(g_sMotorTempRL, &g_Parameters, 0, 0, &g_sKentec320x240x16_SSD2119,
		177, 202, 35, 30,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL,
		ClrBlack, ClrGray, ClrOldLace, &g_sFontApercu20b, "RL" , 0, 0);

//
// this defines the widget that updates the MotorTempRR
//
Canvas(g_sMotorTempRR, &g_Parameters, &g_sMotorTempRL, 0, &g_sKentec320x240x16_SSD2119,
		250, 202, 35, 30,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL,
		ClrBlack, ClrGray, ClrOldLace, &g_sFontApercu20b, "RR", 0, 0);

//
// this defines the widget that updates the MotorTempFL
//
Canvas(g_sMotorTempFL, &g_Parameters, &g_sMotorTempRR, 0, &g_sKentec320x240x16_SSD2119,
		177, 52, 35, 30,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL,
		ClrBlack, ClrGray, ClrOldLace, &g_sFontApercu20b, "FL", 0, 0);

//
// this defines the widget that updates the MotorTempFR
//
Canvas(g_sMotorTempFR, &g_Parameters, &g_sMotorTempFL, 0, &g_sKentec320x240x16_SSD2119,
		250, 52, 35, 30,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL,
		ClrBlack, ClrGray, ClrOldLace, &g_sFontApercu20b, "FR", 0, 0);

//
// this defines the widget that updates the Battery Cell Temperature text status
//
Canvas(g_sLV, &g_Parameters, &g_sMotorTempFR, 0, &g_sKentec320x240x16_SSD2119,
		73, 19, 65, 30,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL,
		ClrBlack, ClrGray, ClrOldLace, &g_sFontApercu28b, "LV", 0, 0);

//
// this defines the widget that updates the Battery Cell High Temperature text status
//
Canvas(g_sHealth, &g_Parameters, &g_sLV, 0, &g_sKentec320x240x16_SSD2119,
		80, 76, 60, 30,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL,
		ClrBlack, ClrGray, ClrOldLace, &g_sFontApercu32b, "Health", 0, 0);

//
// this defines the widget that updates the PACK CURRENT text status
//
Canvas(g_sPackCurrent, &g_Parameters, &g_sHealth, 0, &g_sKentec320x240x16_SSD2119,
		30, 143, 45, 30,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL,
		ClrBlack, ClrGray, ClrOldLace, &g_sFontApercu22b, "I", 0, 0);

//
// this defines the widget that updates the PACK SUMMED VOLTAGE text status
//
Canvas(g_sPackSummedVoltage, &g_Parameters, &g_sPackCurrent, 0, &g_sKentec320x240x16_SSD2119,
		103, 143, 45, 30,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL,
		ClrBlack, ClrGray, ClrOldLace, &g_sFontApercu22b, "V", 0, 0);

//
// this defines the widget that updates the PACK AVG TEMP text status
//
Canvas(g_sPackTemp, &g_Parameters, &g_sPackSummedVoltage, 0, &g_sKentec320x240x16_SSD2119,
		30, 203, 38, 30,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL,
		ClrBlack, ClrGray, ClrOldLace, &g_sFontApercu28b, "AVG TEMP", 0, 0);

//
// this defines the widget that updates the PACK HIGH TEMP text status
//
Canvas(g_sHighTemp, &g_Parameters, &g_sPackTemp, 0, &g_sKentec320x240x16_SSD2119,
		103, 203, 38, 30,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL,
		ClrBlack, ClrGray, ClrOldLace, &g_sFontApercu28b, "HIGH TEMP", 0, 0);

//-----------------------------------------------------------------------------
// Fourth Panel: Faults Screen
//-----------------------------------------------------------------------------

//
// Background Widget for Faults Screen
//
Canvas(g_Faults, 0, 0, g_psFaults + 0, &g_sKentec320x240x16_SSD2119,
		0, 0, 320, 240,
		CANVAS_STYLE_FILL,
		ClrBlack, 0, 0, 0, 0, 0, 0);

//
// Array containing 8 widgets, each can display a single fault
//
tCanvasWidget g_psFaults[] =
{
	CanvasStruct(&g_Faults, g_psFaults + 1, 0, &g_sKentec320x240x16_SSD2119,
			0, 0, 320, 30,
			CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT | CANVAS_STYLE_TEXT_VCENTER |
			CANVAS_STYLE_TEXT_HCENTER,
			ClrCrimson, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0),

	CanvasStruct(&g_Faults, g_psFaults + 2, 0, &g_sKentec320x240x16_SSD2119,
			0, 30, 320, 30,
			CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT | CANVAS_STYLE_TEXT_VCENTER |
			CANVAS_STYLE_TEXT_LEFT,
			ClrBlack, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0),

	CanvasStruct(&g_Faults, g_psFaults + 3, 0, &g_sKentec320x240x16_SSD2119,
			0, 60, 320, 30,
			CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT | CANVAS_STYLE_TEXT_VCENTER |
			CANVAS_STYLE_TEXT_LEFT,
			ClrCrimson, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0),

	CanvasStruct(&g_Faults, g_psFaults + 4, 0, &g_sKentec320x240x16_SSD2119,
			0, 90, 320, 30,
			CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT | CANVAS_STYLE_TEXT_VCENTER |
			CANVAS_STYLE_TEXT_LEFT,
			ClrBlack, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0),

	CanvasStruct(&g_Faults, g_psFaults + 5, 0, &g_sKentec320x240x16_SSD2119,
			0, 120, 320, 30,
			CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT | CANVAS_STYLE_TEXT_VCENTER |
			CANVAS_STYLE_TEXT_LEFT,
			ClrCrimson, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0),

	CanvasStruct(&g_Faults, g_psFaults + 6, 0, &g_sKentec320x240x16_SSD2119,
			0, 150, 320, 30,
			CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT | CANVAS_STYLE_TEXT_VCENTER |
			CANVAS_STYLE_TEXT_LEFT,
			ClrBlack, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0),

	CanvasStruct(&g_Faults, g_psFaults + 7, 0, &g_sKentec320x240x16_SSD2119,
			0, 180, 320, 30,
			CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT | CANVAS_STYLE_TEXT_VCENTER |
			CANVAS_STYLE_TEXT_LEFT,
			ClrCrimson, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0),

	CanvasStruct(&g_Faults, 0, 0, &g_sKentec320x240x16_SSD2119,
			0, 210, 320, 30,
			CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT | CANVAS_STYLE_TEXT_VCENTER |
			CANVAS_STYLE_TEXT_LEFT,
			ClrBlack, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0),
};

//-----------------------------------------------------------------------------
// Fifth Panel: Hardware Diagnostics Screen
//-----------------------------------------------------------------------------

//
// this defines the background widget for the diagnostics screen
//
Canvas(g_Diagnostics, 0, 0, &g_sADC1_Voltage, &g_sKentec320x240x16_SSD2119,
		0, 0, 320, 240,
		CANVAS_STYLE_APP_DRAWN | CANVAS_STYLE_FILL,
		ClrBlack, 0, 0, 0, 0, 0,
		OnHardwareDiagnosticsPaint);

//
// these define the labels for updating the button inputs
//
Canvas(g_sPA0, &g_Diagnostics, 0, 0, &g_sKentec320x240x16_SSD2119,
		45, 5, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPA1, &g_Diagnostics, &g_sPA0, 0, &g_sKentec320x240x16_SSD2119,
		45, 21, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPA3, &g_Diagnostics, &g_sPA1, 0, &g_sKentec320x240x16_SSD2119,
		45, 37, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPA6, &g_Diagnostics, &g_sPA3, 0, &g_sKentec320x240x16_SSD2119,
		45, 53, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPA7, &g_Diagnostics, &g_sPA6, 0, &g_sKentec320x240x16_SSD2119,
		45, 70, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPB0, &g_Diagnostics, &g_sPA7, 0, &g_sKentec320x240x16_SSD2119,
		45, 86, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPB1, &g_Diagnostics, &g_sPB0, 0, &g_sKentec320x240x16_SSD2119,
		45, 102, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPB2, &g_Diagnostics, &g_sPB1, 0, &g_sKentec320x240x16_SSD2119,
		45, 118, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPB3, &g_Diagnostics, &g_sPB2, 0, &g_sKentec320x240x16_SSD2119,
		45, 134, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPB5, &g_Diagnostics, &g_sPB3, 0, &g_sKentec320x240x16_SSD2119,
		45, 150, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPB6, &g_Diagnostics, &g_sPB5, 0, &g_sKentec320x240x16_SSD2119,
		45, 166, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPB7, &g_Diagnostics, &g_sPB6, 0, &g_sKentec320x240x16_SSD2119,
		45, 182, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPD2, &g_Diagnostics, &g_sPB7, 0, &g_sKentec320x240x16_SSD2119,
		45, 198, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPD4, &g_Diagnostics, &g_sPD2, 0, &g_sKentec320x240x16_SSD2119,
		45, 214, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPD5, &g_Diagnostics, &g_sPD4, 0, &g_sKentec320x240x16_SSD2119,
		205, 5, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPD6, &g_Diagnostics, &g_sPD5, 0, &g_sKentec320x240x16_SSD2119,
		205, 21, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPE1, &g_Diagnostics, &g_sPD6, 0, &g_sKentec320x240x16_SSD2119,
		205, 37, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPE2, &g_Diagnostics, &g_sPE1, 0, &g_sKentec320x240x16_SSD2119,
		205, 53, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPE3, &g_Diagnostics, &g_sPE2, 0, &g_sKentec320x240x16_SSD2119,
		205, 70, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPE4, &g_Diagnostics, &g_sPE3, 0, &g_sKentec320x240x16_SSD2119,
		205, 86, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPE5, &g_Diagnostics, &g_sPE4, 0, &g_sKentec320x240x16_SSD2119,
		205, 102, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

Canvas(g_sPF1, &g_Diagnostics, &g_sPE5, 0, &g_sKentec320x240x16_SSD2119,
		205, 118, 100, 16,
		CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
		ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "LOW", 0, 0);

//
// these define labels for updating the analog inputs
//

Canvas(g_sADC0, &g_Diagnostics, &g_sPF1, 0, &g_sKentec320x240x16_SSD2119,
	   215, 166, 50, 16,
	   CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
	   ClrBlack, 0, ClrOldLace, &g_sFontApercu16b, "-----", 0, 0);

Canvas(g_sADC0_Voltage, &g_Diagnostics, &g_sADC0, 0, &g_sKentec320x240x16_SSD2119,
	   215, 182, 50, 16,
	   CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_LEFT,
	   ClrBlack, 50, ClrOldLace, &g_sFontApercu16b, "-----", 0, 0);

Canvas(g_sADC1, &g_Diagnostics, &g_sADC0_Voltage, 0, &g_sKentec320x240x16_SSD2119,
	   215, 198, 50, 16,
	   CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_VCENTER |
		CANVAS_STYLE_TEXT_LEFT,
	   ClrBlack, 50, ClrOldLace, &g_sFontApercu16b, "-----", 0, 0);

Canvas(g_sADC1_Voltage, &g_Diagnostics, &g_sADC1, 0, &g_sKentec320x240x16_SSD2119,
	   215, 214, 50, 16,
	   CANVAS_STYLE_TEXT | CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT_LEFT,
	   ClrBlack, 50, ClrOldLace, &g_sFontApercu16b, "-----", 0, 0);

//-----------------------------------------------------------------------------
// Sixth Panel: Communications Screen
//-----------------------------------------------------------------------------
//Paints background for Communications screen
Canvas(g_Communications, 0, 0, &g_coms_header, &g_sKentec320x240x16_SSD2119,
        0, 0, 320, 240,
        CANVAS_STYLE_FILL,
        ClrBlack, 0, ClrOldLace, &g_sFontApercu22b, 0, 0, 0);

//Paints the ECU label.
Canvas(g_ecu_label, &g_Communications, 0, 0, &g_sKentec320x240x16_SSD2119,
       5, 40, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "ECU Status", 0, 0);

//Paints the ECU status. Initially green, meaning connected; changes to crimson when disconnected.
Canvas(g_ecu_status, &g_Communications, &g_ecu_label, 0, &g_sKentec320x240x16_SSD2119,
       160, 40, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0);

//Paints the BMS label.
Canvas(g_bms_label, &g_Communications, &g_ecu_status, 0, &g_sKentec320x240x16_SSD2119,
       5, 70, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "BMS Status", 0, 0);

//Paints the BMS status. Initially green, meaning connected; changes to crimson when disconnected.
Canvas(g_bms_status, &g_Communications, &g_bms_label, 0, &g_sKentec320x240x16_SSD2119,
       160, 70, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0);

//Paints the SBG (EKF) label.
Canvas(g_sbg_label, &g_Communications, &g_bms_status, 0, &g_sKentec320x240x16_SSD2119,
       5, 100, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "SBG Status", 0, 0);

//Paints the SBG status. Initially green, meaning connected; changes to crimson when disconnected.
Canvas(g_sbg_status, &g_Communications, &g_sbg_label, 0, &g_sKentec320x240x16_SSD2119,
       160, 100, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0);

//Paints the Steering Angle Sensor label.
Canvas(g_sdo_label, &g_Communications, &g_sbg_status, 0, &g_sKentec320x240x16_SSD2119,
       5, 130, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "SAS Status", 0, 0);

//Paints the Steering Angle Sensor status. Initially green, meaning connected; changes to crimson when disconnected.
Canvas(g_sdo_status, &g_Communications, &g_sdo_label, 0, &g_sKentec320x240x16_SSD2119,
       160, 130, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0);

//Paints the DAQ label.
Canvas(g_daq_label, &g_Communications, &g_sdo_status, 0, &g_sKentec320x240x16_SSD2119,
       5, 160, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "DAQ Status", 0, 0);

//Paints the DAQ status. Initially green, meaning connected; changes to crimson when disconnected.
Canvas(g_daq_status, &g_Communications, &g_daq_label, 0, &g_sKentec320x240x16_SSD2119,
       160, 160, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0);

//Paints the Bosch YRS label.
Canvas(g_yrs_label, &g_Communications, &g_daq_status, 0, &g_sKentec320x240x16_SSD2119,
       5, 190, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "YRS Status", 0, 0);

//Paints the Bosch YRS status. Initially green, meaning connected; changes to crimson when disconnected.
Canvas(g_yrs_status, &g_Communications, &g_yrs_label, 0, &g_sKentec320x240x16_SSD2119,
       160, 190, 155, 20,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrGray, 0, ClrOldLace, &g_sFontApercu22b, "", 0, 0);

//Paints the header.
Canvas(g_coms_header, &g_Communications, &g_yrs_status, 0, &g_sKentec320x240x16_SSD2119,
       0, 0, 320, 30,
       CANVAS_STYLE_FILL | CANVAS_STYLE_TEXT,
       ClrCrimson, 0, ClrOldLace, &g_sFontApercu22b, "Communications Status", 0, 0);



/**
 * @brief Paints the Splash Screen background and labels
 *
 * @param *pWidget drawing widget to modify
 * @param *pContext drawing context to modify
 */
void OnSplashPaint(tWidget *pWidget, tContext *pContext)
{
    // Draw Splash Screen Image
    GrImageDraw(pContext, image_pucWisconsinRacingSplash, 0, 0);

    // Introduction Text
    draw_text_centered(pContext, &g_sFontApercu20bi, ClrMediumSeaGreen,
                       "WR-218e", 21, 165, 225, 0);
}

/**
 * @brief Paints the Driver Screen background and labels
 *
 * @param *pWidget drawing widget to modify
 * @param *pContext drawing context to modify
 */
void OnDriverPaint(tWidget *pWidget, tContext *pContext)
{
	// BATTERY GRAPHIC Image
    GrImageDraw(pContext, image_pucBatteryGraphic, 5, 5);

	// % label for CHARGE STATUS
    GrImageDraw(pContext, image_pucChargeBorder, 5, 55);
	draw_text(pContext, &g_sFontApercu36b, ClrGray, "%", -1, 123, 90, 0);

	// mph label
	GrImageDraw(pContext, image_pucSpeedBorder, 170, 55);
	draw_text(pContext, &g_sFontApercu22b, ClrGray, "mph", -1, 265, 104, 0);

	// TORQUE VECTORING label
    draw_text(pContext, &g_sFontApercu20b, ClrGray, "RTD", -1, 170, 155, 0);

	// TORQUE VECTORING label
    draw_text(pContext, &g_sFontApercu22b, ClrGray, "TV", -1, 175, 184, 0);

	// TRACTION CONTROL label
    draw_text(pContext, &g_sFontApercu22b, ClrGray, "TC", -1, 175, 214, 0);
}

/**
 * @brief Paints the Parameters Screen background and labels
 *
 * @param *pWidget drawing widget to modify
 * @param *pContext drawing context to modify
 */
void OnParametersPaint(tWidget *pWidget, tContext *pContext)
{
	// BATTERY PACK STATUS label
//    draw_text_vertical(pContext, &g_sFontApercu22b, ClrGray, "BATTERY", 7, 5, 5, 20, 0);
//    draw_text_vertical(pContext, &g_sFontApercu22b, ClrGray, "PACK", 4, 5, 155, 20, 0); TODO: this function needs to be fixed in the common library

	// MOTOR TEMPERATURE label
    draw_text(pContext, &g_sFontApercu22b, ClrGray, "MOTOR", -1, 205, 5, 0);
    draw_text(pContext, &g_sFontApercu22b, ClrGray, "TEMPERATURE", -1, 170, 25, 0);

    // CAR ICON Image
    GrImageDraw(pContext, image_pucCarIcon, 192, 95);

	// LOW V label
    draw_text(pContext, &g_sFontApercu16b, ClrGray, "LV", -1, 35, 16, 0);
    draw_text(pContext, &g_sFontApercu16b, ClrGray, "Batt", -1, 35, 32, 0);

	draw_text(pContext, &g_sFontApercu24b, ClrGray, "V", -1, 140, 25, 0);

	// HEALTH label TODO: No longer applicable
//    draw_text(pContext, &g_sFontApercu16b, ClrGray, "Pack", -1, 35, 74, 0);
//    draw_text(pContext, &g_sFontApercu16b, ClrGray, "Health", -1, 35, 90, 0);
	draw_text(pContext, &g_sFontApercu16b, ClrGray, "Ignore", -1, 35, 82, 0);

	draw_text(pContext, &g_sFontApercu24b, ClrGray, "%", -1, 143, 83, 0);

	// PACK CURRENT label
    draw_text(pContext, &g_sFontApercu16b, ClrGray, "Current", -1, 32, 125, 0);

    draw_text(pContext, &g_sFontApercu18b, ClrGray, "A", -1, 77, 150, 0);

	// PACK VOLTAGE label
    draw_text(pContext, &g_sFontApercu16b, ClrGray, "Voltage", -1, 105, 125, 0);

    draw_text(pContext, &g_sFontApercu18b, ClrGray, "V", -1, 149, 150, 0);

	// AVG TEMP label
    draw_text(pContext, &g_sFontApercu16b, ClrGray, "Avg", -1, 34, 185, 0);

    draw_text(pContext, &g_sFontApercu20b, ClrGray, "C", -1, 75, 211, 0);

	draw_text(pContext, &g_sFontApercu16b, ClrGray, "o", -1, 68, 200, 0);

	// HIGH TEMP label
    draw_text(pContext, &g_sFontApercu16b, ClrGray, "High", -1, 107, 185, 0);

    draw_text(pContext, &g_sFontApercu20b, ClrGray, "C", -1, 148, 211, 0);

	draw_text(pContext, &g_sFontApercu16b, ClrGray, "o", -1, 142, 200, 0);

	//  MOTOR TEMPERARTURE C labels
	// probably more efficient to not use draw_ functions here
	GrContextFontSet(pContext, &g_sFontApercu18b);
	GrContextForegroundSet(pContext, ClrGray);
	GrStringDraw(pContext, "C", -1, 220, 60, 0);
	GrStringDraw(pContext, "C", -1, 295, 60, 0);
	GrStringDraw(pContext, "C", -1, 220, 210, 0);
	GrStringDraw(pContext, "C", -1, 295, 210, 0);
	GrContextFontSet(pContext, &g_sFontApercu12b);
	GrContextForegroundSet(pContext, ClrGray);
	GrStringDraw(pContext, "o", -1, 212, 55, 0);
	GrStringDraw(pContext, "o", -1, 287, 55, 0);
	GrStringDraw(pContext, "o", -1, 212, 205, 0);
	GrStringDraw(pContext, "o", -1, 287, 205, 0);

	// Color Translate for DpyLineDraw function
	uint64_t ClrBorder = DpyColorTranslate(&g_sKentec320x240x16_SSD2119, ClrGray);

	// BATTERY PACK PARAMETERS borders

	// AVG TEMP
	draw_boarders(pContext, ClrBorder, 25, 5, 140, 53);

	// HIGH TEMP
	draw_boarders(pContext, ClrBorder, 25, 63, 140, 53);

	// PACK CURRENT
	draw_boarders(pContext, ClrBorder, 25, 121, 68, 53);

	// PACK SUMMED VOLTAGE
	draw_boarders(pContext, ClrBorder, 98, 121, 67, 53);

	// LOW VOLTAGE
	draw_boarders(pContext, ClrBorder, 25, 179, 68, 56);

	// HIGH VOLTAGE
	draw_boarders(pContext, ClrBorder, 98, 179, 67, 56);

	// MOTOR TEMPERATURE borders
	draw_boarders(pContext, ClrBorder, 170, 50, 70, 35);
	draw_boarders(pContext, ClrBorder, 245, 50, 70, 35);
	draw_boarders(pContext, ClrBorder, 170, 200, 70, 35);
	draw_boarders(pContext, ClrBorder, 245, 200, 70, 35);
}

/**
 * @brief Paints the Battery Bar Graphic
 *
 * @param *pWidget drawing widget to modify
 * @param *pContext drawing context to modify
 */
void OnBatteryGraphicPaint(tWidget *pWidget, tContext *pContext)
{
	uint16_t charge = CANParams.bmsSOC.soc;

	if (charge == 0)
	{
		return;
	}

	// Round up to a multiple of 10
	if (charge != 100)
	{
		charge = ((charge + 10) / 10) * 10;
	}

	switch (charge)
	{
		case 100:	GrImageDraw(pContext, image_pucBatteryBlock100, 14, 14);
					break;
		case 90:	GrImageDraw(pContext, image_pucBatteryBlock90, 14, 14);
					break;
		case 80:	GrImageDraw(pContext, image_pucBatteryBlock80, 14, 14);
					break;
		case 70:	GrImageDraw(pContext, image_pucBatteryBlock70, 14, 14);
					break;
		case 60:	GrImageDraw(pContext, image_pucBatteryBlock60, 14, 14);
					break;
		case 50:	GrImageDraw(pContext, image_pucBatteryBlock50, 14, 14);
					break;
		case 40:	GrImageDraw(pContext, image_pucBatteryBlock40, 14, 14);
					break;
		case 30:	GrImageDraw(pContext, image_pucBatteryBlock30, 14, 14);
					break;
		case 20:	GrImageDraw(pContext, image_pucBatteryBlock20, 14, 14);
					break;
		case 10:	GrImageDraw(pContext, image_pucBatteryBlock10, 14, 14);
					break;
		default:	break;
	}
}

/**
 * @brief Warning Triangles on Driver screen
 *
 * @param *pWidget drawing widget to modify
 * @param *pContext drawing context to modify
 *
 * A dark grey triangle is painted if there are no faults
 * A colored (red/orange/yellow) triangle is painted if there
 * are faults in the corresponding category
 */
void OnFaultsWarningPaint(tWidget *pWidget, tContext *pContext)
{
	// current fault/flag statuses
	uint32_t ECUMotorFaults1 = CANParams.ecuFlags.ECUMotorFlags1;
	uint32_t ECUMotorFaults2 = CANParams.ecuFlags.ECUMotorFlags2;
	uint32_t ECUOtherFaults = CANParams.ecuFlags.ECUOtherFlags;

	// Paint RED Triangle (Motor Faults)
	if (0 == ECUMotorFaults1 && 0 == ECUMotorFaults2)
	{
		GrImageDraw(pContext, image_pucNoFaults, 5, 190);
	}
	else
	{
		GrImageDraw(pContext, image_pucFaultTriangleRed, 5, 190);
	}

	// Paint ORANGE Triangle (Other Faults)
	if (0 == ECUOtherFaults)
	{
		GrImageDraw(pContext, image_pucNoFaults, 60, 190);
	}
	else
	{
		GrImageDraw(pContext, image_pucFaultTriangleOrange, 60, 190);
	}

	// Paint YELLOW Triangle (BMS Faults) TODO: Find another use for this.
//	if (BMS_FAULT1 == 0 && BMS_FAULT2 == 0)
//	{
//		GrImageDraw(pContext, image_pucNoFaults, 115, 190);
//	}
//	else
//	{
//		GrImageDraw(pContext, image_pucFaultTriangleYellow, 115, 190);
//	}
}

/**
 * @brief Labels on the Hardware Diagnostics Screen
 *
 * @param *pWidget drawing widget to modify
 * @param *pContext drawing context to modify
 */
void OnHardwareDiagnosticsPaint(tWidget *pWidget, tContext *pContext)
{
    // Probably better to not use Lib_draw here
	GrContextFontSet(pContext, &g_sFontApercu16b);
	GrContextForegroundSet(pContext, ClrGray);

	//Digital labels
	GrStringDraw(pContext, "PA0:", -1, 5, 5, 0);
	GrStringDraw(pContext, "PA1:", -1, 5, 21, 0);
	GrStringDraw(pContext, "PA3:", -1, 5, 37, 0);
	GrStringDraw(pContext, "PA6:", -1, 5, 53, 0);
	GrStringDraw(pContext, "PA7:", -1, 5, 70, 0);
	GrStringDraw(pContext, "PB0:", -1, 5, 86, 0);
	GrStringDraw(pContext, "PB1:", -1, 5, 102, 0);
	GrStringDraw(pContext, "PB2:", -1, 5, 118, 0);
	GrStringDraw(pContext, "PB3:", -1, 5, 134, 0);
	GrStringDraw(pContext, "PB5:", -1, 5, 150, 0);
	GrStringDraw(pContext, "PB6:", -1, 5, 166, 0);
	GrStringDraw(pContext, "PB7:", -1, 5, 182, 0);
	GrStringDraw(pContext, "PD2:", -1, 5, 198, 0);
	GrStringDraw(pContext, "PD4:", -1, 5, 214, 0);
	GrStringDraw(pContext, "PD5:", -1, 165, 5, 0);
	GrStringDraw(pContext, "PD6:", -1, 165, 21, 0);
	GrStringDraw(pContext, "PE1:", -1, 165, 37, 0);
	GrStringDraw(pContext, "PE2:", -1, 165, 53, 0);
	GrStringDraw(pContext, "PE3:", -1, 165, 70, 0);
	GrStringDraw(pContext, "PE4:", -1, 165, 86, 0);
	GrStringDraw(pContext, "PE5:", -1, 165, 102, 0);
	GrStringDraw(pContext, "PF1:", -1, 165, 118, 0);

	//Analog labels
	GrStringDraw(pContext, "ADC0:", -1, 165, 166, 0);
	GrStringDraw(pContext, "V:", -1, 197, 182, 0);

	GrStringDraw(pContext, "ADC1:", -1, 165, 198, 0);
	GrStringDraw(pContext, "V:", -1, 197, 214, 0);

	// Color translate for DpyLineDraw
	uint64_t ClrBorder = DpyColorTranslate(&g_sKentec320x240x16_SSD2119, ClrGray);

	// Dividing line
	DpyLineDrawV(&g_sKentec320x240x16_SSD2119, 160, 0, 240, ClrBorder);
}

/**
 * @brief hardware initialization and startup graphics
 */
void GraphicsInit(void)
{
    draw_graphics_init(&sContext, (tWidget*)&g_splashScreen, (tWidget*)&g_Driver);
}

/**
 * @brief handles change screen logic
 *
 * Called when the change screen button is pressed
 */
void LCDChangeScreen()
{
	// Keeps track of whether all faults has been checked or not
	static bool allFaultsChecked = true;

	// if on DRIVER screen then go to PARAMTERS screen
	if (curr == DRIVER)
	{
		WidgetPaint((tWidget *)(&g_Parameters));
		WidgetMessageQueueProcess();
		LCDUpdateParametersScreen();

		curr = PARAMETERS;
	}

	// if on parameters screen then go to FAULTS screen
	else if (curr == PARAMETERS)
	{
		// Painting is handled inside this method
		allFaultsChecked = LCDUpdateFaultsScreen();

		curr = FAULTS;
	}

	// if on FAULTS screen
	else if (curr == FAULTS)
	{
		// Not all faults has been checked, more to display
		if (!allFaultsChecked)
		{
			allFaultsChecked = LCDUpdateFaultsScreen();
		}

		// All faults has been checked, no more to display
		else
		{
			WidgetPaint((tWidget *)(&g_Diagnostics));
			WidgetMessageQueueProcess();
			LCDUpdateHardwareDiagnosticsScreen();

			curr = DIAGNOSTICS;
		}
	}
	else if (curr == DIAGNOSTICS) {

		WidgetPaint((tWidget *)(&g_Communications));
		WidgetMessageQueueProcess();
		LCDUpdateCommunicationsScreen();

		curr = COMMUNICATIONS;
	}

		else if (curr == COMMUNICATIONS)
	{
        WidgetPaint((tWidget *)(&g_Driver));
        WidgetMessageQueueProcess();
        LCDUpdateDriverScreen();

	    curr = DRIVER;
	}
}

/**
 * @breif calls function to update parameters on screen
 */
void LCDUpdateScreen(void)
{
	if (curr == DRIVER)
	{
		LCDUpdateDriverScreen();
	}
	else if (curr == PARAMETERS)
	{
		LCDUpdateParametersScreen();
	}
	else if (curr == DIAGNOSTICS)
	{
		LCDUpdateHardwareDiagnosticsScreen();
	}
	else if (curr == COMMUNICATIONS)
	{
	    LCDUpdateCommunicationsScreen();
	}
}

/**
 * @brief updates parameters on driver screen
 */
void LCDUpdateDriverScreen(void)
{
	ChargePaint();
	SpeedPaint();
	ECUDriverFlagsPaint();
	BatteryGraphicPaint();
	FaultsWarningPaint();
}

/**
 * @brief updates parameters on parameters screen
 */
void LCDUpdateParametersScreen(void)
{
	PackTempPaint();
	HighTempPaint();
	PackCurrentPaint();
	PackSummedVoltagePaint();
	LVPaint();
	HealthPaint();
	MotorTemperaturesPaint();
}

/**
 * @brief updates parameters on parameters screen
 */
void LCDUpdateHardwareDiagnosticsScreen(void)
{

	// Store the old previous values to determine if changed
	static uint16_t buttonsOld = 0xFFFF;

	static uint16_t analog0Old = 0xFFFF;
	static uint16_t analog1Old = 0xFFFF;

	// Fetch the values stored in Wheel_Buttons.c. Values are updated in main.
	uint16_t buttons = GetButtons();

	uint16_t analog0 = GetAnalog0();
	uint16_t analog1 = GetAnalog1();


	// Set all the labels to current values if the input changes.

	if (buttons != buttonsOld)
	{
		DigitalInputPaint(&g_sPA0, buttons & (1 << INPUT_PA0));
		DigitalInputPaint(&g_sPA1, buttons & (1 << INPUT_PA1));
		DigitalInputPaint(&g_sPA3, buttons & (1 << INPUT_PA3));
		DigitalInputPaint(&g_sPA6, buttons & (1 << INPUT_PA6));
		DigitalInputPaint(&g_sPA7, buttons & (1 << INPUT_PA7));
		DigitalInputPaint(&g_sPB0, buttons & (1 << INPUT_PB0));
		DigitalInputPaint(&g_sPB1, buttons & (1 << INPUT_PB1));
		DigitalInputPaint(&g_sPB2, buttons & (1 << INPUT_PB2));
		DigitalInputPaint(&g_sPB3, buttons & (1 << INPUT_PB3));
		DigitalInputPaint(&g_sPB5, buttons & (1 << INPUT_PB5));
		DigitalInputPaint(&g_sPB6, buttons & (1 << INPUT_PB6));
		DigitalInputPaint(&g_sPB7, buttons & (1 << INPUT_PB7));
		DigitalInputPaint(&g_sPD2, buttons & (1 << INPUT_PD2));
		DigitalInputPaint(&g_sPD4, buttons & (1 << INPUT_PD4));
		DigitalInputPaint(&g_sPD5, buttons & (1 << INPUT_PD5));
		DigitalInputPaint(&g_sPD6, buttons & (1 << INPUT_PD6));
		DigitalInputPaint(&g_sPE1, buttons & (1 << INPUT_PE1));
		DigitalInputPaint(&g_sPE2, buttons & (1 << INPUT_PE2));
		DigitalInputPaint(&g_sPE3, buttons & (1 << INPUT_PE3));
		DigitalInputPaint(&g_sPE4, buttons & (1 << INPUT_PE4));
		DigitalInputPaint(&g_sPE5, buttons & (1 << INPUT_PE5));
		DigitalInputPaint(&g_sPF1, buttons & (1 << INPUT_PF1));

		// Update buttonsOld when buttons changes
		buttonsOld = buttons;
	}

	// Set the analog values only when they change
	if (analog0 != analog0Old)
	{

		// Update analog0 count
		sprintf(pcAnalog0Text, "%i", analog0);
		CanvasTextSet(&g_sADC0, pcAnalog0Text);
		WidgetPaint((tWidget *)&g_sADC0);
		WidgetMessageQueueProcess();

		// Voltage (mV) = (Input mV / steps) * count
		uint16_t voltage0 = (5000 / 4095) * analog0;

		// Update voltage
		sprintf(pcAnalog0VoltageText, "%i", voltage0);
		CanvasTextSet(&g_sADC0_Voltage, pcAnalog0VoltageText);
		WidgetPaint((tWidget *)&g_sADC0_Voltage);
		WidgetMessageQueueProcess();

		// Update analog0Old if changed
		analog0Old = analog0;
	}

	if (analog1 != analog1Old)
	{
		// Update analog1 count
		sprintf(pcAnalog1Text, "%i", analog1);
		CanvasTextSet(&g_sADC1, pcAnalog1Text);
		WidgetPaint((tWidget *)&g_sADC1);
		WidgetMessageQueueProcess();

		// Voltage (mV) = (Input mV / steps) * count
		uint16_t voltage1 = (5000 / 4095) * analog1;

		sprintf(pcAnalog1VoltageText, "%i", voltage1);
		CanvasTextSet(&g_sADC1_Voltage, pcAnalog1VoltageText);
		WidgetPaint((tWidget *)&g_sADC1_Voltage);
		WidgetMessageQueueProcess();

		// Update analog1Old if changed
		analog1Old = analog1;
	}
}

/**
 * @brief Updates the Communications Screen
 * @author Mostafa Hassan
 */
void LCDUpdateCommunicationsScreen()
{
    //Old values - only updates screen if it changes
    static bool ECU_old;
    static bool BMS_old;
    static bool SBG_old;
    static bool SAS_old;
    static bool DAQ_old;
    static bool YRS_old;

    //disconnect times
    static float BMS_disconnect = 1.0;
    static float DAQ_disconnect = 1.0;
    static float ECU_disconnect = 1.0;
    static float SBG_disconnect = 1.0;
    static float SAS_disconnect = 1.0;
    static float YRS_disconnect = 1.0;

    //This method must run at least once and update everything
    static bool has_run = false;
    if(!has_run)
    {
        ECU_old = !ECU;
        BMS_old = !BMS;
        SBG_old = !SBG;
        SAS_old = !SAS;
        DAQ_old = !DAQ;
        YRS_old = !YRS;
        has_run = true;
    }

    //Update ECU Communication Status
    if(ECU)
    {
        //reset variables
        ECU = false;
        ECU_disconnect = 0;

        if(!ECU_old)
        {
            CanvasTextSet(&g_ecu_status, "Connected");
            CanvasFillColorSet(&g_ecu_status, ClrGreen);
            WidgetPaint((tWidget *) &g_ecu_status);
            WidgetMessageQueueProcess();

            ECU_old = true;

        }
    }
    else if(ECU_old)
    {

        if(ECU_disconnect >= 1.0)
        {
            CanvasTextSet(&g_ecu_status, "Disconnected");
            CanvasFillColorSet(&g_ecu_status, ClrCrimson);
            WidgetPaint((tWidget *) &g_ecu_status);
            WidgetMessageQueueProcess();

            ECU_old = false;
        }
        else
        {
            ECU_disconnect += 0.1; // should increment every 100 ms
        }

    }

    //Update BMS Communication Status
    if(BMS)
    {
        //reset variables
        BMS = false;
        BMS_disconnect = 0;

        if(!BMS_old)
        {
            CanvasTextSet(&g_bms_status, "Connected");
            CanvasFillColorSet(&g_bms_status, ClrGreen);
            WidgetPaint((tWidget *) &g_bms_status);
            WidgetMessageQueueProcess();

            BMS_old = true;

        }
    }else if(BMS_old)
    {

        if(BMS_disconnect >= 1.0)
        {
            CanvasTextSet(&g_bms_status, "Disconnected");
            CanvasFillColorSet(&g_bms_status, ClrCrimson);
            WidgetPaint((tWidget *) &g_bms_status);
            WidgetMessageQueueProcess();

            BMS_old = false;
        }
        else
        {
            BMS_disconnect += 0.1; // should increment every 100 ms
        }

    }

    //Update SBG Communication Status
    if(SBG)
    {
        //reset variables
        SBG = false;
        SBG_disconnect = 0;

        if(!SBG_old)
        {
            CanvasTextSet(&g_sbg_status, "Connected");
            CanvasFillColorSet(&g_sbg_status, ClrGreen);
            WidgetPaint((tWidget *) &g_sbg_status);
            WidgetMessageQueueProcess();

            SBG_old = true;

        }
    }else if(SBG_old)
    {

        if(SBG_disconnect >= 1.0)
        {
            CanvasTextSet(&g_sbg_status, "Disconnected");
            CanvasFillColorSet(&g_sbg_status, ClrCrimson);
            WidgetPaint((tWidget *) &g_sbg_status);
            WidgetMessageQueueProcess();

            SBG_old = false;
        }
        else
        {
            SBG_disconnect += 0.1; // should increment every 100 ms
        }

    }

    //Update SAS Communication Status
    if(SAS)
    {
        //reset variables
        SAS = false;
        SAS_disconnect = 0;

        if(!SAS_old)
        {
            CanvasTextSet(&g_sdo_status, "Connected");
            CanvasFillColorSet(&g_sdo_status, ClrGreen);
            WidgetPaint((tWidget *) &g_sdo_status);
            WidgetMessageQueueProcess();

            SAS_old = true;

        }
    }else if(!SAS && SAS_old)
    {

        if(SAS_disconnect >= 1.0)
        {
            CanvasTextSet(&g_sdo_status, "Disconnected");
            CanvasFillColorSet(&g_sdo_status, ClrCrimson);
            WidgetPaint((tWidget *) &g_sdo_status);
            WidgetMessageQueueProcess();

            SAS_old = false;
        }
        else
        {
            SAS_disconnect += 0.1; // should increment every 100 ms
        }

    }

    //Update DAQ Communication Status
    if(DAQ)
    {
        //reset variables
        DAQ = false;
        DAQ_disconnect = 0;

        if(!DAQ_old)
        {
            CanvasTextSet(&g_daq_status, "Connected");
            CanvasFillColorSet(&g_daq_status, ClrGreen);
            WidgetPaint((tWidget *) &g_daq_status);
            WidgetMessageQueueProcess();

            DAQ_old = true;

        }
    }else if(DAQ_old)
    {

        if(DAQ_disconnect >= 1.0)
        {
            CanvasTextSet(&g_daq_status, "Disconnected");
            CanvasFillColorSet(&g_daq_status, ClrCrimson);
            WidgetPaint((tWidget *) &g_daq_status);
            WidgetMessageQueueProcess();

            DAQ_old = false;
        }
        else
        {
            DAQ_disconnect += 0.1; // should increment every 100 ms
        }

    }

    //Update YRS Communication Status
    if(YRS)
    {
        //reset variables
        YRS = false;
        YRS_disconnect = 0;

        if(!YRS_old)
        {
            CanvasTextSet(&g_yrs_status, "Connected");
            CanvasFillColorSet(&g_yrs_status, ClrGreen);
            WidgetPaint((tWidget *) &g_yrs_status);
            WidgetMessageQueueProcess();

            YRS_old = true;

        }
    }else if(YRS_old)
    {

        if(YRS_disconnect >= 1.0)
        {
            CanvasTextSet(&g_yrs_status, "Disconnected");
            CanvasFillColorSet(&g_yrs_status, ClrCrimson);
            WidgetPaint((tWidget *) &g_yrs_status);
            WidgetMessageQueueProcess();

            YRS_old = false;
        }
        else
        {
            YRS_disconnect += 0.1; // should increment every 100 ms
        }

    }

}

/**
 * @brief sets the widget text to ON or OFF in the correct color.
 *
 *
 */
void DigitalInputPaint(tCanvasWidget* pWidget, int value)
{
	if (value)
	{
		CanvasTextSet(pWidget, "ON");
		CanvasTextColorSet(pWidget, ClrMediumSeaGreen);
	}
	else
	{
		CanvasTextSet(pWidget, "OFF");
		CanvasTextColorSet(pWidget, ClrCrimson);
	}
	WidgetPaint((tWidget *) pWidget);
	WidgetMessageQueueProcess();
}

/**
 * @breif scrolls through active faults
 *
 * @returns true if there are more faults to be display, false if there is not
 *
 * Is called when the faults screen is active. This function will display 7
 * faults per screen. If there is more than 7 faults active on the next press
 * of the change screens button, it will display the next up to 7 faults that
 * are active until all faults have been cycled through. Once all faults are
 * cycled through, it will let the screen go to the next screen.
 */
bool LCDUpdateFaultsScreen(void)
{
	// keep track of the fault category
	static int faultCategory = 0;

	// keep track of the fault index, loops through all fault flags
	static int faultIndex = 0;

	// keep track of the line number for displaying the faults strings
	int line = 1;

	// local loop variable
	int i = 0;

	// store faults variables gotten from CAN
    uint32_t ECUMotorFaults1 = CANParams.ecuFlags.ECUMotorFlags1;
    uint32_t ECUMotorFaults2 = CANParams.ecuFlags.ECUMotorFlags2;
    uint32_t ECUOtherFaults = CANParams.ecuFlags.ECUOtherFlags;

	char sFaultText[25];

	//
	// Condition executes if there are no faults detected
	//
	if (noFaults())
	{

		// print No Faults Detected on the top line
		sprintf(sFaultText, "No Faults Detected");
		CanvasTextSet(g_psFaults + 0, sFaultText);

		// clear the text from the rest of the lines
		for (i = 1; i < 8; i++)
		{
			CanvasTextSet(g_psFaults + i, "");
		}

		// apply changes to the screen
		WidgetPaint((tWidget *)(&g_Faults));
		WidgetMessageQueueProcess();

		// clear fault Category and Index
		faultCategory = 0;
		faultIndex = 0;

		// Return true to indicate that we are "done" checking faults
		return true;
	}

	//
	// If we make it here, then faults are active
	// faultIndex and faultCategory = 0 means that it is the first time looping,
	// set first line to "Faults Active"
	//
	if ((faultIndex == 0) && (faultCategory == 0))
	{
		// print "--Faults Active--" on the top
		sprintf(sFaultText, "--Faults Active--");
		CanvasTextSet(g_psFaults + 0, "--Faults Active--");
		WidgetPaint((tWidget *)(g_psFaults + 0));
		WidgetMessageQueueProcess();
	}

	//
	// If we make it here then faults are active. Line 0 will display "Faults
	// Active" on it and line = 1. Loop through the faults variables to find the
	// active faults. If a fault is active then display the corresponding string
	// and increase the line. If lines run out before we loop through all of the
	// faults variables then return true. If we reach the end of the the faults
	// variables then return false.
	//

	//
	// ECU MOTOR FAULTS - 40 faults
	//
	if (faultCategory == 0)
	{
		// go through ECUMotorFaults1 - 32 faults
		while (line < 8 && faultIndex < 32)
		{

			// if fault is active display it
			if (ECUMotorFaults1 & (1 << faultIndex))
			{
				// set text to corresponding text message in the array
				CanvasTextSet((g_psFaults + line), faults_msg[faultCategory][faultIndex]);
				WidgetPaint((tWidget *)(g_psFaults + line));
				WidgetMessageQueueProcess();

				// incr line if we placed a fault on the line
				line++;
			}

			// increment fault index
			faultIndex++;
		}

		// go through ECUMotorFaults2 - 8 faults
		while (line < 8 && faultIndex < 40)
		{
			// if fault is active display it
			if (ECUMotorFaults2 & (1 << (faultIndex - 32)))
			{
				// set text to corresponding text message in the array
				CanvasTextSet((g_psFaults + line), faults_msg[faultCategory][faultIndex]);
				WidgetPaint((tWidget *)(g_psFaults + line));
				WidgetMessageQueueProcess();

				// incr line if we placed a fault on the line
				line++;
			}

			// increment fault index
			faultIndex++;
		}

		// All motor faults has been checked/displayed
		if (faultIndex >= 40)
		{
			// Advance fault category and reset fault index
			faultCategory = 1;
			faultIndex = 0;
		}
	}

	//
	// ECU OTHER FAULTS - 32 faults
	//
	if (faultCategory == 1)
	{
		while (line < 8 && faultIndex < 32)
		{
			// if fault is active display it
			if (ECUOtherFaults & (1 << faultIndex))
			{
				// set text to corresponding text message in the array
				CanvasTextSet((g_psFaults + line), faults_msg[faultCategory][faultIndex]);
				WidgetPaint((tWidget *)(g_psFaults + line));
				WidgetMessageQueueProcess();

				// incr line if we placed a fault on the line
				line++;
			}

			// increment fault index
			faultIndex++;
		}

		// All motor faults has been checked/displayed
//		if (faultIndex >= 32)
//		{
//			// Advance fault category and reset fault index
//			faultCategory = 2;
//			faultIndex = 0;
//		}
	}

	//
	// BMS FAULTS - 40 faults
	//
//	if (faultCategory == 2)
//	{
//		// go through BMSFaults1 - 32 faults
//		while (line < 8 && faultIndex < 32)
//		{
//			// if fault is active display it
//			if (BMS_FAULT1 & (1 << faultIndex))
//			{
//				// set text to corresponding text message in the array
//				CanvasTextSet((g_psFaults + line), faults_msg[faultCategory][faultIndex]);
//				WidgetPaint((tWidget *)(g_psFaults + line));
//				WidgetMessageQueueProcess();
//
//				// incr line if we placed a fault on the line
//				line++;
//			}
//
//			// increment fault index
//			faultIndex++;
//		}
//
//		// go through BMSFaults2 - 8 faults
//		while (line < 8 && faultIndex < 40)
//		{
//			// if fault is active display it
//			if (BMS_FAULT2 & (1 << (faultIndex - 32)))
//			{
//				// set text to corresponding text message in the array
//				CanvasTextSet((g_psFaults + line), faults_msg[faultCategory][faultIndex]);
//				WidgetPaint((tWidget *)(g_psFaults + line));
//				WidgetMessageQueueProcess();
//
//				// incr line if we placed a fault on the line
//				line++;
//			}
//
//			// increment fault index
//			faultIndex++;
//		}
//	}

	//
	// Condition executes if all faults are checked
	// All faults are checked when we reach index 32 of category 1
	//
	if (faultCategory == 1 && faultIndex >= 32)
	{
		// clear the text from the rest of the lines without faults
		while (line < 8)
		{
			CanvasTextSet(g_psFaults + line, "");
			WidgetPaint((tWidget *)(g_psFaults + line));
			WidgetMessageQueueProcess();

			line++;
		}

		// Reset category and index
		faultCategory = 0;
		faultIndex = 0;

		// Return true to indicate that we are "done" checking for faults
		return true;
	}

	//
	// Return false to indicate that there we are not "done" checking for
	// faults, and that there are more faults to be displayed
	//
	return false;
}

/**
 * @brief Paints Battery Bar Graphic
 *
 * By painting the BatteryGraphic Widget, the function @c OnBatteryGraphicPaint()
 * is called.
 */
void BatteryGraphicPaint(void)
{
	static uint16_t chargeOld = 69;

	uint16_t charge = CANParams.bmsSOC.soc;

	if (charge == 0)
	{
		return;
	}

	// Round to multiple of 10
	if (charge != 100)
	{
		charge = ((charge + 10) / 10) * 10;
	}

	// Only paint if charge (rounded to multiple of 10) has changed
	if (charge != chargeOld)
	{
		WidgetPaint((tWidget *)(&g_sBatteryGraphic));
		WidgetMessageQueueProcess();
		chargeOld = charge;
	}
}

/**
 * @brief Paints the 3 fault triangles
 */
void FaultsWarningPaint(void)
{
	WidgetPaint((tWidget *)(&g_sFaultsWarning));
	WidgetMessageQueueProcess();
}

/**
 * @brief Updates charge on driver screen
 */
void ChargePaint(void)
{
	static uint16_t chargeIntOld = 69;

	uint16_t chargeInt = CANParams.bmsSOC.soc;

	if (chargeInt != chargeIntOld)
	{
		if (chargeInt >= 30)
		{
			CanvasTextColorSet(&g_sPackStateOfCharge, ClrMediumSeaGreen);
		}
		else if (chargeInt >= 20)
		{
			CanvasTextColorSet(&g_sPackStateOfCharge, ClrGold);
		}
		else if (chargeInt >= 10)
		{
			CanvasTextColorSet(&g_sPackStateOfCharge, ClrSandyBrown);
		}
		else
		{
			CanvasTextColorSet(&g_sPackStateOfCharge, ClrCrimson);
		}

		sprintf(pcPackStateOfChargeText, "%i", chargeInt);
		CanvasTextSet(&g_sPackStateOfCharge, pcPackStateOfChargeText);
		WidgetPaint((tWidget *)&g_sPackStateOfCharge);
		WidgetMessageQueueProcess();

		chargeIntOld = chargeInt;
	}
}

/**
 * @brief Updates the speed on the driver screen
 */
void SpeedPaint(void)
{
	static uint16_t speedOld = 0xFFFF;

	//
	// Get the value of the vehicle's speed
	//
	uint16_t speed = GetSpeed();

	//
	// Check to see if the value has changed, if it has updated it
	//
	if(speed != speedOld)
	{
		//
		// Convert the speed integer to text and then update
		// the screen with the speed text
		//
		sprintf(pcSpeedText, "%i", speed);
		CanvasTextSet(&g_sSpeed, pcSpeedText);
		WidgetPaint((tWidget *)&g_sSpeed);
		WidgetMessageQueueProcess();

		//
		// Reset the old values so the values can be updated when they change
		//
		speedOld = speed;
	}

}

/**
 * @brief Updates flags on the Driver Screen
 */
void ECUDriverFlagsPaint(void)
{
	static uint32_t readyOld = 0xFFFFFFFF;
	static uint8_t TVold = 0xFF;

	uint32_t ready = CANParams.ecuFlags.ECUPerformanceFlags & ECU_FLAG_READY_TO_DRIVE;
	uint8_t TV = CANParams.daq.torqueVectoring;

	//
	// READY TO DRIVE
	//
	if (ready != readyOld)
	{
		if (ready != 0)
		{
			CanvasTextSet(&g_sReady, "READY");
			CanvasFillColorSet(&g_sReady, ClrMediumSeaGreen);
		}
		else
		{
			CanvasTextSet(&g_sReady, "NO");
			CanvasFillColorSet(&g_sReady, ClrCrimson);
		}

		WidgetPaint((tWidget *)&g_sReady);
		WidgetMessageQueueProcess();

		readyOld = ready;
	}

//	//
//	// TORQUE VECTORING
//	//
	if (TV != TVold)
	{
		if (0 != TV)
		{
			CanvasTextSet(&g_sTorqueVectoring, "ON");
			CanvasFillColorSet(&g_sTorqueVectoring, ClrMediumSeaGreen);
		}
		else
		{
			CanvasTextSet(&g_sTorqueVectoring, "OFF");
			CanvasFillColorSet(&g_sTorqueVectoring, ClrCrimson);
		}

		WidgetPaint((tWidget *)&g_sTorqueVectoring);
		WidgetMessageQueueProcess();

		TVold = TV;
	}
}

/**
 * @brief Updates dial flags on the driver screen
 *
 * @param buttons inputs from steering wheel
 *
 * Updates the Launch Control and Drive Mode flags based on the switches on the
 * steering wheel
 */
void LCDUpdateDialFlags(uint16_t buttons)
{

	static uint16_t buttonsOld = 0xFFFF;

	//
	// Update only if button states has changed and in DRIVER screen
	//
	if ( (buttons != buttonsOld) && (curr == DRIVER) )
	{
		//
		// DRIVING MODE
		//
		if (buttons & BUTTONS_ENDURANCE_MODE_M)
		{
			CanvasFontSet(&g_sMode, &g_sFontApercu22b);
			CanvasTextSet(&g_sMode, "ENDURANCE");
			CanvasFillColorSet((&g_sMode), ClrSandyBrown);
		}
		else if (buttons & BUTTONS_ACCELERATION_MODE_M)
		{
			CanvasFontSet(&g_sMode, &g_sFontApercu20b);
			CanvasTextSet(&g_sMode, "ACCELERATION");
			CanvasFillColorSet((&g_sMode), ClrCrimson);
		}
		else if (buttons & BUTTONS_AUTOCROSS_MODE_M)
		{
			CanvasFontSet(&g_sMode, &g_sFontApercu22b);
			CanvasTextSet(&g_sMode, "AUTOCROSS");
			CanvasFillColorSet((&g_sMode), ClrGold);
		}
		else if (buttons & BUTTONS_SKIDPAD_MODE_M)
		{
			CanvasFontSet(&g_sMode, &g_sFontApercu22b);
			CanvasTextSet(&g_sMode, "SKIDPAD");
			CanvasFillColorSet((&g_sMode), ClrMediumSeaGreen);
		}

		WidgetPaint((tWidget *)(&g_sMode));
		WidgetMessageQueueProcess();

		//
		// TRACTION CONTROL
		//
		if (buttons & BUTTONS_TC_MIN_M)
		{
			CanvasTextSet(&g_sTractionControl, "MIN");
			CanvasFillColorSet((&g_sTractionControl), ClrMediumSeaGreen);
		}
		else if (buttons & BUTTONS_TC_MID_M)
		{
			CanvasTextSet(&g_sTractionControl, "MID");
			CanvasFillColorSet((&g_sTractionControl), ClrMediumSeaGreen);
		}
		else if (buttons & BUTTONS_TC_MAX_M)
		{
			CanvasTextSet(&g_sTractionControl, "MAX");
			CanvasFillColorSet((&g_sTractionControl), ClrMediumSeaGreen);
		}
		else
		{
			CanvasFillColorSet(&g_sTractionControl, ClrCrimson);
			CanvasTextSet(&g_sTractionControl, "OFF");
		}

		WidgetPaint((tWidget *)(&g_sTractionControl));
		WidgetMessageQueueProcess();

		buttonsOld = buttons;
	}
}

/**
 * @brief Updates the voltage on the Parameters screen
 *
 * Uses Pack Summed Voltage
 */
void PackSummedVoltagePaint(void)
{
	static uint16_t PackSummedVoltageOld = 0;

	uint16_t PackSummedVoltage = CANParams.bmsSOC.soc;

	if (PackSummedVoltage != PackSummedVoltageOld)
	{
		sprintf(pcPackSummedVoltageText, "%i", PackSummedVoltage);
		CanvasTextSet(&g_sPackSummedVoltage, pcPackSummedVoltageText);
		WidgetPaint((tWidget *)&g_sPackSummedVoltage);
		WidgetMessageQueueProcess();

		PackSummedVoltageOld = PackSummedVoltage;
	}
}

/**
 * @brief Updates the Pack Current on the Parameters screen
 */
void PackCurrentPaint(void)
{
	static uint16_t PackCurrentOld = 0;

	uint16_t PackCurrent = CANParams.bmsPackStats.packCurrent;

	if (PackCurrent != PackCurrentOld)
	{
		sprintf(pcPackCurrentText, "%i", PackCurrent);
		CanvasTextSet(&g_sPackCurrent, pcPackCurrentText);
		WidgetPaint((tWidget *)&g_sPackCurrent);
		WidgetMessageQueueProcess();

		PackCurrentOld = PackCurrent;
	}
}

/**
 * @brief Updates the temperature on the Parameters screen
 *
 * Uses the average cell temperature
 */
void PackTempPaint(void)
{
	static uint8_t cellTempOld = 69;

	uint8_t cellTemp = CANParams.bmsVitals.avgTemp;

	if (cellTemp != cellTempOld)
	{
		sprintf(pcPackTempText, "%i", cellTemp);
		CanvasTextSet(&g_sPackTemp, pcPackTempText);
		WidgetPaint((tWidget *)&g_sPackTemp);
		WidgetMessageQueueProcess();

		cellTempOld = cellTemp;
	}
}

/**
 * @brief Updates the high cell temperature on the parameters screen
 */
void HighTempPaint(void)
{
	static int8_t highTempOld = 69;

	int8_t highTemp = CANParams.bmsVitals.highTemp;

	if (highTemp != highTempOld)
	{
		sprintf(pcHighTempText, "%i", highTemp);
		CanvasTextSet(&g_sHighTemp, pcHighTempText);
		WidgetPaint((tWidget *)&g_sHighTemp);
		WidgetMessageQueueProcess();

		highTempOld = highTemp;
	}
}

/**
 * @brief Updates the Low Voltage on the Parameters Screen
 */
void LVPaint(void)
{
	static uint8_t lvIntOld = 69;
	static uint8_t lvDecOld = 69;

	uint8_t lvInt = CANParams.daq.lvInt;
	uint8_t lvDec = CANParams.daq.lvDec;

	if (lvInt != lvIntOld ||
		lvDec != lvDecOld)
	{
		sprintf(pcLVText, "%i.%i", lvInt, lvDec);
		CanvasTextSet(&g_sLV, pcLVText);
		WidgetPaint((tWidget *)&g_sLV);
		WidgetMessageQueueProcess();

		lvIntOld = lvInt;
		lvDecOld = lvDec;
	}
}

/**
 * @brief Updates the Pack SOH on the Parameters Screen
 * TODO: No longer needed - replace with more useful functionality
 */
void HealthPaint(void)
{
	static uint8_t healthOld = 69;

	uint8_t health = 0;

	if (health != healthOld)
	{
		sprintf(pcHealthText, "%i", health);
		CanvasTextSet(&g_sHealth, pcHealthText);
		WidgetPaint((tWidget *)&g_sHealth);
		WidgetMessageQueueProcess();

		healthOld = health;
	}
}

/**
 * @brief Updates the Motor Temperature on the Parameters screen
 */
void MotorTemperaturesPaint(void)
{
	static uint8_t motorTempFLOld = 0;
	static uint8_t motorTempFROld = 0;
	static uint8_t motorTempRLOld = 0;
	static uint8_t motorTempRROld = 0;

	uint8_t motorTempFL = CANParams.motorTemps.motorTempFL;
	uint8_t motorTempFR = CANParams.motorTemps.motorTempFR;
	uint8_t motorTempRL = CANParams.motorTemps.motorTempRL;
	uint8_t motorTempRR = CANParams.motorTemps.motorTempRR;

	//
	// FL MOTOR TEMP
	//
	if (motorTempFL != motorTempFLOld)
	{
		if (motorTempFL >= MOTOR_RED_TEMP)
		{
			CanvasTextColorSet(&g_sMotorTempFL, ClrCrimson);
		}
		else
		{
			CanvasTextColorSet(&g_sMotorTempFL, ClrOldLace);
		}

		sprintf(pcMotorTempFLText, "%i", motorTempFL);
		CanvasTextSet(&g_sMotorTempFL, pcMotorTempFLText);
		WidgetPaint((tWidget *)&g_sMotorTempFL);
		WidgetMessageQueueProcess();

		motorTempFLOld = motorTempFL;
	}

	//
	// FR MOTOR TEMP
	//
	if (motorTempFR != motorTempFROld)
	{
		if (motorTempFR >= MOTOR_RED_TEMP)
		{
			CanvasTextColorSet(&g_sMotorTempFR, ClrCrimson);
		}
		else
		{
			CanvasTextColorSet(&g_sMotorTempFR, ClrOldLace);
		}

		sprintf(pcMotorTempFRText, "%i", motorTempFR);
		CanvasTextSet(&g_sMotorTempFR, pcMotorTempFRText);
		WidgetPaint((tWidget *)&g_sMotorTempFR);
		WidgetMessageQueueProcess();

		motorTempFROld = motorTempFR;
	}

	//
	// RL MOTOR TEMP
	//
	if (motorTempRL != motorTempRLOld)
	{
		if (motorTempRL >= MOTOR_RED_TEMP)
		{
			CanvasTextColorSet(&g_sMotorTempRL, ClrCrimson);
		}
		else
		{
			CanvasTextColorSet(&g_sMotorTempRL, ClrOldLace);
		}

		sprintf(pcMotorTempRLText, "%i", motorTempRL);
		CanvasTextSet(&g_sMotorTempRL, pcMotorTempRLText);
		WidgetPaint((tWidget *)&g_sMotorTempRL);
		WidgetMessageQueueProcess();

		motorTempRLOld = motorTempRL;
	}

	//
	// RR MOTOR TEMP
	//
	if (motorTempRR != motorTempRROld)
	{
		if (motorTempRR >= MOTOR_RED_TEMP)
		{
			CanvasTextColorSet(&g_sMotorTempRR, ClrCrimson);
		}
		else
		{
			CanvasTextColorSet(&g_sMotorTempRR, ClrOldLace);
		}

		sprintf(pcMotorTempRRText, "%i", motorTempRR);
		CanvasTextSet(&g_sMotorTempRR, pcMotorTempRRText);
		WidgetPaint((tWidget *)&g_sMotorTempRR);
		WidgetMessageQueueProcess();

		motorTempRROld = motorTempRR;
	}
}
