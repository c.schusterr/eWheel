/**
 * @brief main
 *
 * This file is the top level file for this project.  It executes all
 * initialization functions and provides the main loop for the program
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "timers_ewheel.h"
#include "inputs_ewheel.h"
#include "can_ewheel.h"
#include "lcd_ewheel.h"
#include "watchdog_ewheel.h"
#include "rgb_ewheel.h"

/**
 * @brief enable clocking for all GPIO ports
 */
void MainGPIOPeriphInit(void)
{
	// enable clocking
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

	// wait for clocking to be ready
	while (	(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOA))
			&& (!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB))
			&& (!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOC))
			&& (!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD))
			&& (!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE))
			&& (!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF))
	) {};
}

/**
 * @brief Call peripheral initializations
 */
void MainInit(void)
{
    // disable all interrupts
    IntMasterDisable();

    MainGPIOPeriphInit();
    GraphicsInit();
    Timer0AInit();
    timer2A_init();
    DigitalInputInit();
    CANInitialize();
    RGBInit();
    RGBTurnOffAll();
    // TODO WatchdogInitialize(); - enable before release
    AnalogInputInit();

    // Enable interrupts
	IntMasterEnable();
}

/**
 * @brief main
 */
int main(void)
{
	//*************************************************************************
	//
	// Variables
	//
	//*************************************************************************
	uint32_t buttons = 0x0000;		//!< store button states
	uint32_t buttonsOld = 0xFFFF;	//!< comparison to previous button states
	uint16_t analog0 = 0;
	uint16_t analog1 = 0;

    // Set the clocking to run directly from the external crystal/oscillator.
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    MainInit();

    while (1)
    {
		//---------------------------------------------------------------------
		// 1 ms wheel routine
		//---------------------------------------------------------------------
    	if (timerFlags & FLAG_1MS)
    	{
    		// feed the dog every 1ms
    		// watchdog triggers in ~1 second
    		// TODO WatchdogIntClear(WATCHDOG0_BASE); - enable before release

    	    InputDebounce();
    		buttons = GetButtons();

    		//
			// Handle Change Screen Button Press
			//
			if ((buttons & CHANGE_SCREENS_M) && (buttons != buttonsOld))
			{
				LCDChangeScreen();
			}

    		//
    		// Update static button variable if buttons has changed
    		//
    		if (buttons != buttonsOld)
    		{
    			buttonsOld = buttons;
    		}

    		timerFlags &= ~FLAG_1MS;
    	} // end of 1 ms routine

		//---------------------------------------------------------------------
		// 10 ms wheel routine
		//---------------------------------------------------------------------
		if(timerFlags & FLAG_10MS)
		{
			// read analog values
		    AnalogInputRead(&analog0, &analog1);

		    // Send button values over CAN
			//CANTxButtons(buttons, analog0, analog1);

			// Update Dial Flags
			LCDUpdateDialFlags(buttons);

			// Update RGB Bar
			RGBUpdate();

			timerFlags &= ~FLAG_10MS;
		} // end of 10 ms routine

		//---------------------------------------------------------------------
		// 100 ms wheel routine
		//---------------------------------------------------------------------
		if(timerFlags & FLAG_100MS)
		{
			// Update parameters on the screen
			LCDUpdateScreen();

			timerFlags &= ~FLAG_100MS;

			// Send button values over CAN
			CANTxButtons(buttons, analog0, analog1);
		} // end of 100 ms routine

    	//---------------------------------------------------------------------
    	// Call the CAN data handler to take care of any pending RX or TX messages
    	//---------------------------------------------------------------------
    	CANDataHandler();

    }  // end of running while loop
} // end of main class
