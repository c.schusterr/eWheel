/**
 * @brief Contains functions for initializing the timers
 */

#include <stdbool.h>
#include <stdint.h>
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "timers_ewheel.h"

//*****************************************************************************
//
// Global variables for this file
//
//*****************************************************************************
volatile uint8_t timerFlags;	//!< keep track of timed interrupts

/**
 * @brief Initialize timer0
 */
void Timer0AInit(void)
{
	// Enable the timer0 peripheral
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

	// Configure the two 32-bit periodic timers.  Configured to 1ms
	TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC); // full-width periodic timer
	TimerLoadSet(TIMER0_BASE, TIMER_A, SysCtlClockGet() / 1000); // TIMER_A is a ulTimer parameter and exists in the timer.h library

	// Setup the interrupts for the timer timeouts.
	TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT); //TIMER_TIMA_TIMEOUT defined in timer.h
	IntEnable(INT_TIMER0A); // in hw_ints.h

	// Enable the timers and clear interrupt.
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	TimerEnable(TIMER0_BASE, TIMER_A);
}

/**
 * @brief timer0 interrupt handler
 */
__interrupt void Timer0IntHandler(void)
{
	// counters for timer flags
	static volatile uint8_t		ms10;
	static volatile uint8_t		ms100;
	static volatile uint8_t		ms250;
	static volatile uint8_t		ms1000;

    // Clear the timer interrupt.
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    // Disable interrupts while we calculate which timer has been reached
    IntMasterDisable();

	// 0.001 second flag (1ms flag)
	timerFlags |= FLAG_1MS;	// C |= 2 is same as C = C | 2

	// 0.01 second flag
	if (ms10++ > 9) // 10 times
	{
		timerFlags |= FLAG_10MS;
		ms10 = 1;
	}

	// 0.1 second flag
	if (ms100++ > 99 ) // 100 times
	{
	    timerFlags |= FLAG_100MS;
	    ms100=1;
	}

	// Re-enable interrupts
    IntMasterEnable();
}

/**
 * @brief Initialize timer2A as 32-bit periodic counting up. This timer is used
 * for determining if timeouts occur.
 *
 */
void timer2A_init(void)
{
    //
    // Enable clocking
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);

    // Wait for clocking to be ready
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER2)) {};

    //
    // Configure timer
    //
    TimerConfigure(TIMER2_BASE, TIMER_CFG_PERIODIC_UP); // 32-bit periodic timer
    TimerLoadSet(TIMER2_BASE, TIMER_A, TIMER_MAX); // start counting from 0

    //
    // Enable peripheral
    //
    TimerEnable(TIMER2_BASE, TIMER_A);
}

uint32_t get_time(void)
{
    return TimerValueGet(TIMER2_BASE, TIMER_A);
}

/**
 * Compares the time given from the past to the current time.
 *
 * @param time - time to compare to current time
 * @param timeout - constraint on the comparison
 * @returns true if current_time - time > timeout and false if otherwise
 */
bool timeout_occured(uint32_t *time, uint32_t timeout)
{
    uint32_t current_time = get_time();
    uint32_t difference;

    if (*time <= current_time)
    {
        difference = current_time - *time;
    }
    else
    {
        difference = (TIMER_MAX - *time) + current_time;
    }

    return (difference > timeout);
}
