/**
 * @brief Handles CAN sending and receiving
 *
 * Sends and receives CAN messages. Extracts the data and places it into
 * variables to be used to update the LCD screen.
 */

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_can.h"
#include "inc/hw_gpio.h"
#include "inc/hw_ints.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/can.h"
#include "driverlib/pin_map.h"
#include "Lib_can.h"
#include "timers_ewheel.h"
#include "can_ewheel.h"

//*****************************************************************************
//
// Global variables for this file
//
//*****************************************************************************

//-----------------------------------------------------------------------------
// CAN Timeout Variables
//-----------------------------------------------------------------------------
bool BMS_timeout = false;           // indicates a BMS timeout occurred
bool ECU_timeout = false;           // indicates a ECU timeout occurred
bool EKS_timeout = false;           // indicates a EKS timeout occurred
bool DAQ_timeout = false;           // indicates a DAQ timeout occurred
bool Motor_timeout = false;         // indicates a Motor_Controller timeout occurred

bool BMS = false;
bool DAQ = false;
bool ECU = false;
bool SBG = false;
bool SAS = false;
bool YRS = false;
bool Motor_Controller = false;

//-----------------------------------------------------------------------------
// Variables to hold the parameters and their initial display values
//-----------------------------------------------------------------------------
extern CAN_Params CANParams;

/**
 * @brief Initialize CAN Message Objects
 */
static void CANMsgObjInit(void)
{
    // TX

    // Initialize Buttons TX CAN Object
    can_create_message(CAN_TX_BUTTONS_OBJ_ID, CAN_BUTTONS_MSG_ID, CAN_BUTTONS_LENGTH, MSG_OBJ_TX_INT_ENABLE, CAN_TX);

	// RX

	// Initialize BMS Vitals CAN Object
    can_create_message(CAN_RX_BMS_VITALS_OBJ_ID, CAN_RX_BMS_VITALS_MSG_ID, CAN_RX_BMS_VITALS_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

    // Initialize BMS SOC CAN Object
    can_create_message(CAN_RX_BMS_SOC_OBJ_ID, CAN_RX_BMS_SOC_MSG_ID, CAN_RX_BMS_SOC_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

	// Initialize BMS Pack Stats CAN Object
    can_create_message(CAN_RX_BMS_PACK_STATS_OBJ_ID, CAN_RX_BMS_PACK_STATS_MSG_ID, CAN_RX_BMS_PACK_STATS_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

    // Initialize Aux MCU Digital Message
    can_create_message(CAN_RX_AUX_DIG_OBJ_ID, CAN_RX_AUX_DIG_MSG_ID, CAN_RX_AUX_DIG_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

    // Initialize Aux MCU Analog Message
    can_create_message(CAN_RX_AUX_ANALOG_OBJ_ID, CAN_RX_AUX_ANALOG_MSG_ID, CAN_RX_AUX_ANALOG_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

	// Initialize ECU Motor Flags CAN Object
    can_create_message(CAN_RX_ECU_MOTOR_FLAGS_OBJ_ID, CAN_RX_ECU_MOTOR_FLAGS_MSG_ID, CAN_RX_ECU_MOTOR_FLAGS_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

	// Initialize ECU Other Flags CAN Object
    can_create_message(CAN_RX_ECU_OTHER_FLAGS_OBJ_ID, CAN_RX_ECU_OTHER_FLAGS_MSG_ID, CAN_RX_ECU_OTHER_FLAGS_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

	// Initialize ECU Performance Flags CAN Object
    can_create_message(CAN_RX_ECU_PERFORMANCE_FLAGS_OBJ_ID, CAN_RX_ECU_PERFORMANCE_FLAGS_MSG_ID, CAN_RX_ECU_PERFORMANCE_FLAGS_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

	// Initialize the Bosch YRS CAN Object
    can_create_message(CAN_RX_BOSCH_YRS_1_OBJ_ID, CAN_RX_BOSCH_YRS_1_MSG_ID, CAN_RX_BOSCH_YRS_1_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

    // Initialize EKF VEL CAN Object
    can_create_message(CAN_RX_EKF_VEL_OBJ_ID, CAN_RX_EKF_VE_MSGL_ID, CAN_RX_EKF_VEL_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

    // Initialize SDO CAN Object
    can_create_message(CAN_RX_SDO_RESP_OBJ_ID, CAN_RX_SDO_RESP_MSG_ID, CAN_RX_SDO_RESP_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

	// Initialize DAQ 13 CAN Object
    can_create_message(CAN_RX_DAQ_13_OBJ_ID, CAN_RX_DAQ_13_MSG_ID, CAN_RX_DAQ_13_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

	// Initialize DAQ 29 CAN Object
    can_create_message(CAN_RX_DAQ_29_OBJ_ID, CAN_RX_DAQ_29_MSG_ID, CAN_RX_DAQ_29_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

	// Initialize Motor Controller FL CAN Object
    can_create_message(CAN_RX_CONTROLLER_FL_OBJ_ID, CAN_RX_CONTROLLER_FL_MSG_ID, CAN_RX_CONTROLLER_FL_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

	// Initialize Motor Controller FR CAN Object
    can_create_message(CAN_RX_CONTROLLER_FR_OBJ_ID, CAN_RX_CONTROLLER_FR_MSG_ID, CAN_RX_CONTROLLER_FR_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

	// Initialize Motor Controller RL CAN Object
    can_create_message(CAN_RX_CONTROLLER_RL_OBJ_ID, CAN_RX_CONTROLLER_RL_MSG_ID, CAN_RX_CONTROLLER_RL_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);

	// Initialize Motor Controller RR CAN Object
    can_create_message(CAN_RX_CONTROLLER_RR_OBJ_ID, CAN_RX_CONTROLLER_RR_MSG_ID, CAN_RX_CONTROLLER_RR_LENGTH, MSG_OBJ_RX_INT_ENABLE, CAN_RX);
}

/**
 * @brief Initialize the CAN Peripheral
 *
 * Initialize CAN peripheral and calls the function to initialize the CAN
 * message objects.
 */
void CANInitialize(void)
{
    can_wheel_init(1000000);

	// Initialize message objects
	CANMsgObjInit();
}

/*
 * This function sets the Time Variables based on Timeout Messages
 */
void timeout(bool msg, uint32_t *prevTime, bool *timeoutVar)
{
    // update timeout time only if new data was received
    if (msg)
    {
        *prevTime = get_time();

        // just came back from CAN timeout - let msg know we regained connectivity
        if (*timeoutVar)
        {
            *timeoutVar = false;
        }
    }

    // check for a receive timeout
    // only send once on trigger
    if (timeout_occured(prevTime, TIMER_50ms) && !*timeoutVar)
    {
        *timeoutVar = true; // Setting the timeout to true for the given msg if timeout occurs
    }
}

//-----------------------------------------------------------------------------
// CAN Parsing Methods
//-----------------------------------------------------------------------------

/**
 * @brief Parses BMS_VITALS CAN Message
 */
void can_bms_vitals_parse(uint8_t canRxBMSVitals[])
{
    CANParams.bmsVitals.highTemp = canRxBMSVitals[4] * CAN_SIG_CELL_HIGH_TEMP;
    CANParams.bmsVitals.avgTemp = canRxBMSVitals[7] * CAN_SIG_CELL_AVG_TEMP;
    CANParams.bmsVitals.highCellVolt = canRxBMSVitals[5] * CAN_SIG_CELL_HIGH_VOLT;
    CANParams.bmsVitals.lowCellVolt = canRxBMSVitals[6] * CAN_SIG_CELL_LOW_VOLT;
}

/**
 * @brief Parses BMS Pack CAN Message
 */
void can_bms_pack_parse(uint8_t canRxBMSPackStats[])
{
    CANParams.bmsPackStats.packSummedVoltage = ((canRxBMSPackStats[3] << 8) + canRxBMSPackStats[2]) * CAN_SIG_PACK_SUMMED_VOLTAGE;
    CANParams.bmsPackStats.packCurrent = ((canRxBMSPackStats[1] << 8) + canRxBMSPackStats[0]) * CAN_SIG_PACK_CURRENT;
}

/**
 * @brief Parses Aux MCU Digital Data
 */
void can_aux_dig_parse(uint8_t canRxAuxDig[])
{
    CANParams.auxDig.airs_closed = (canRxAuxDig[0] >> 7) && 0xFF;
    CANParams.auxDig.ecu_fault = (canRxAuxDig[0] >> 6) && 0xFF;
    CANParams.auxDig.sdc_supply = (canRxAuxDig[0] >> 5) && 0xFF;
    CANParams.auxDig.sdc_return = (canRxAuxDig[0] >> 4) && 0xFF;
    CANParams.auxDig.imd_fault = (canRxAuxDig[0] >> 3) && 0xFF;
    CANParams.auxDig.bpd_fault = (canRxAuxDig[0] >> 2) && 0xFF;
    CANParams.auxDig.mux_batt_valid = (canRxAuxDig[0] >> 1) && 0xFF;
    CANParams.auxDig.mux_dcdc_valid = canRxAuxDig[0] && 0xFF;
    CANParams.auxDig.bms_fault = canRxAuxDig[1] && 0xFF;
}

/**
 * @brief Parses Aux MCU Analog Data
 */
void can_aux_analog_parse(uint8_t canRxAuxAnalog[])
{
    CANParams.auxAnalog.sdc_supply_v = canRxAuxAnalog[0] * CAN_SIG_SDC_SUPPLY_V;
    CANParams.auxAnalog.sdc_return_v = canRxAuxAnalog[1] * CAN_SID_SDC_RETURN_V;
    CANParams.auxAnalog.lv_dcdc_v = canRxAuxAnalog[2] * CAN_SID_LV_DCDC_V;
    CANParams.auxAnalog.five_v = canRxAuxAnalog[3] * CAN_SID_FIVE_V;
    CANParams.auxAnalog.brk_pres1_v = canRxAuxAnalog[4] * CAN_SID_BRK_PRES1_V;
    CANParams.auxAnalog.imd_fault_v = canRxAuxAnalog[5] * CAN_SID_IMD_FAULT_V;
    CANParams.auxAnalog.air_coil_v = canRxAuxAnalog[6] * CAN_SID_AIR_COIL_V;
    CANParams.auxAnalog.lv_batt_v = canRxAuxAnalog[7] * CAN_SID_LV_BATT_V;
}

/**
 * @brief Parses ECU Motor Flags
 */
void can_ecu_motor_flags_parse(uint8_t canRxECUMotorFlags[])
{
    CANParams.ecuFlags.ECUMotorFlags1 = (((uint32_t)canRxECUMotorFlags[0]) << 0);
    CANParams.ecuFlags.ECUMotorFlags1 += ((uint32_t)((uint32_t)canRxECUMotorFlags[1]) << 8);
    CANParams.ecuFlags.ECUMotorFlags1 += ((uint32_t)((uint32_t)canRxECUMotorFlags[2]) << 16);
    CANParams.ecuFlags.ECUMotorFlags1 += ((uint32_t)((uint32_t)canRxECUMotorFlags[3]) << 24);

    CANParams.ecuFlags.ECUMotorFlags2 = (((uint32_t)canRxECUMotorFlags[4]) << 0);
    CANParams.ecuFlags.ECUMotorFlags2 += (((uint32_t)canRxECUMotorFlags[5]) << 8);
    CANParams.ecuFlags.ECUMotorFlags2 += (((uint32_t)canRxECUMotorFlags[6]) << 16);
    CANParams.ecuFlags.ECUMotorFlags2 += (((uint32_t)canRxECUMotorFlags[7]) << 24);
}

/**
 * @brief Parses ECU Other Flags
 */
void can_ecu_other_flags_parse(uint8_t canRxECUOtherFlags[])
{
    CANParams.ecuFlags.ECUOtherFlags = (((uint32_t)canRxECUOtherFlags[0]) << 0);
    CANParams.ecuFlags.ECUOtherFlags += (((uint32_t)canRxECUOtherFlags[1]) << 8);
    CANParams.ecuFlags.ECUOtherFlags += (((uint32_t)canRxECUOtherFlags[2]) << 16);
    CANParams.ecuFlags.ECUOtherFlags += (((uint32_t)canRxECUOtherFlags[3]) << 24);
}

/**
 * @brief Parses ECU Performance Flags
 */
void can_ecu_performance_flags_parse(uint8_t canRxECUPerformanceFlags[])
{
    CANParams.ecuFlags.ECUPerformanceFlags = (((uint32_t)canRxECUPerformanceFlags[0]) << 0);
    CANParams.ecuFlags.ECUPerformanceFlags += (((uint32_t)canRxECUPerformanceFlags[1]) << 8);
    CANParams.ecuFlags.ECUPerformanceFlags += (((uint32_t)canRxECUPerformanceFlags[2]) << 16);
    CANParams.ecuFlags.ECUPerformanceFlags += (((uint32_t)canRxECUPerformanceFlags[3]) << 24);
}

/**
 * @brief Parses EKF VEL Data
 */
void can_ekf_vel_parse(uint8_t canRxEKFVel[])
{
    CANParams.ekfVel.velocity_N = canRxEKFVel[4];
    CANParams.ekfVel.velocity_N += (((uint16_t)(canRxEKFVel[5])) << 8);
    CANParams.ekfVel.velocity_E = canRxEKFVel[2];
    CANParams.ekfVel.velocity_E += (((uint16_t)(canRxEKFVel[3])) << 8);
}

//-----------------------------------------------------------------------------
// Main CAN Handler
//-----------------------------------------------------------------------------

/**
 * @brief Parse incoming CAN data
 *
 * This functions extracts data and scales it from the incoming CAN messages.
 * Clears the interrupt on the pending messages.
 */
void CANDataHandler(void)
{
	// Initialize local variables for function
	static uint32_t ui32Status;
	static uint32_t lastBMSTime = 0;
	static uint32_t lastECUTime = 0;
	static uint32_t lastEKSTime = 0;
	static uint32_t lastDAQTime = 0;
	static uint32_t lastMotorTime = 0;

	// Read the CAN interrupt status to find the cause of the interrupt
	ui32Status = CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE);

	// If the cause is a controller status interrupt, then get the status
	if(ui32Status == CAN_INT_INTID_STATUS)
	{
		CANIntClear(CAN0_BASE, 1);

		// Read the controller status.  This will return a field of status
		// error bits that can indicate various errors.
		ui32Status = CANStatusGet(CAN0_BASE, CAN_STS_CONTROL);
	}

	// Check if the cause is message object 1, which is used for sending
	// message 1.
	if(CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_TX_BUTTONS_OBJ_ID)
	{
		// Getting to this point means that the TX interrupt occurred on
		// message object 1, and the message TX is complete.  Clear the
		// message object interrupt.
		CANIntClear(CAN0_BASE, CAN_TX_BUTTONS_OBJ_ID);
	}

/*
 * BMS Messages
 */
	//-------------------------------------------------------------------------
	// Check if BMS Vitals Message Received
	// CAN Message ID 0x40
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_BMS_VITALS_OBJ_ID)
	{
	    BMS = true;

	    // Read CAN message and clear interrupt
	    uint8_t canRxBMSVitals[CAN_RX_BMS_VITALS_LENGTH];
	    can_read_message(CAN_RX_BMS_VITALS_OBJ_ID, canRxBMSVitals);
	    can_bms_vitals_parse(canRxBMSVitals);
	}

	//-------------------------------------------------------------------------
	// Check if BMS SOC Message Received
	// CAN Message ID 0x43
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_BMS_SOC_OBJ_ID)
	{
	    BMS = true;

	    // Read CAN message and clear interrupt
	    uint8_t canRxBMSSOC[CAN_RX_BMS_SOC_LENGTH];
	    can_read_message(CAN_RX_BMS_SOC_OBJ_ID, canRxBMSSOC);

	    CANParams.bmsSOC.soc = canRxBMSSOC[0] * CAN_SIG_SOC_PERCENT;
	}

	//-------------------------------------------------------------------------
	// check if BMS Pack Stats Message Received
	// CAN Message ID 0x41
	//-------------------------------------------------------------------------
	if(CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_BMS_PACK_STATS_OBJ_ID)
	{
	    BMS = true;
		// Read CAN message and clear interrupt
	    uint8_t canRxBMSPackStats[CAN_RX_BMS_PACK_STATS_LENGTH];
	    can_read_message(CAN_RX_BMS_PACK_STATS_OBJ_ID, canRxBMSPackStats);

	    can_bms_pack_parse(canRxBMSPackStats);
	}

/*
 * Aux MCU Messages
 */
	//-------------------------------------------------------------------------
	// Check if Aux Digital Message Received
	// CAN Message ID 0x30
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_AUX_DIG_OBJ_ID)
	{
	    // Read CAN message and clear interrupt
	    uint8_t canRxAuxDig[CAN_RX_AUX_DIG_LENGTH];
	    can_read_message(CAN_RX_AUX_DIG_OBJ_ID, canRxAuxDig);
	    can_aux_dig_parse(canRxAuxDig);
	}
	//-------------------------------------------------------------------------
	// Check if Aux Analog Message Received
	// CAN Message ID 0x130
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_AUX_ANALOG_OBJ_ID)
	{
	    // Read CAN message and clear interrupt
	    uint8_t canRxAuxAnalog[CAN_RX_AUX_ANALOG_LENGTH];
	    can_read_message(CAN_RX_AUX_ANALOG_OBJ_ID, canRxAuxAnalog);
	    can_aux_analog_parse(canRxAuxAnalog);
	}

/*
 * ECU Messages
 */
	//-------------------------------------------------------------------------
	// check if ECU Motor Flags message received
	// CAN Message ID 0xF1
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_ECU_MOTOR_FLAGS_OBJ_ID)
	{
	    ECU = true;
		// Read CAN message and clear interrupt
        uint8_t canRxECUMotorFlags[CAN_RX_ECU_MOTOR_FLAGS_LENGTH];
        can_read_message(CAN_RX_ECU_MOTOR_FLAGS_OBJ_ID, canRxECUMotorFlags);

		//
		// extract data and perform any scaling or shifting
		//
        can_ecu_motor_flags_parse(canRxECUMotorFlags);
	}

	//-------------------------------------------------------------------------
	// check if ECU Other Flags message received
	// CAN Message ID 0xF2
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_ECU_OTHER_FLAGS_OBJ_ID)
	{
        ECU = true;
		// Read CAN message and clear interrupt
        uint8_t canRxECUOtherFlags[CAN_RX_ECU_OTHER_FLAGS_LENGTH];
        can_read_message(CAN_RX_ECU_OTHER_FLAGS_OBJ_ID, canRxECUOtherFlags);

		//
		// extract data and perform any scaling or shifting
		//
		can_ecu_other_flags_parse(canRxECUOtherFlags);
	}

	//-------------------------------------------------------------------------
	// check if ECU Performance Flags message received
	// CAN Message ID 0xF3
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_ECU_PERFORMANCE_FLAGS_OBJ_ID)
	{
        ECU = true;
		// Read CAN message and clear interrupt
        uint8_t canRxECUPerformanceFlags[CAN_RX_ECU_PERFORMANCE_FLAGS_LENGTH];
        can_read_message(CAN_RX_ECU_PERFORMANCE_FLAGS_OBJ_ID, canRxECUPerformanceFlags);

		can_ecu_performance_flags_parse(canRxECUPerformanceFlags);
	}

/*
 * Bosch YRS Message
 */
	//-------------------------------------------------------------------------
	// check if Bosch Message 1 Received
	// CAN Message ID 0x70
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_BOSCH_YRS_1_OBJ_ID)
	{
	    YRS = true;
	    CANIntClear(CAN0_BASE, CAN_RX_BOSCH_YRS_1_OBJ_ID);
	}

/*
 * SBG Messages
 */
	//-------------------------------------------------------------------------
	// check if EKF VEL Message Received
	// CAN Message ID 0x137
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_EKF_VEL_OBJ_ID)
	{
        SBG = true;
		// Read CAN message and clear interrupt
        uint8_t canRxEKFVel[CAN_RX_EKF_VEL_LENGTH];
        can_read_message(CAN_RX_EKF_VEL_OBJ_ID, canRxEKFVel);

		//
		// extract data and perform any scaling or shifting
		// the two velocity parameters are used to calculate speed
		// calculation and shifting is done in the GetSpeed function
		//
        can_ekf_vel_parse(canRxEKFVel);
	}

/*
 * Steering Angle Sensor Message
 */
	//-------------------------------------------------------------------------
	// check if SDO Response Message Received
	// CAN Message ID 0x5FF
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_SDO_RESP_OBJ_ID)
	{
	    SAS = true;
	    CANIntClear(CAN0_BASE, CAN_RX_SDO_RESP_OBJ_ID);
	}

/*
 * DAQ Messages
 */
	//-------------------------------------------------------------------------
	// check if DAQ 13 Message Received
	// CAN Message ID 0x313
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_DAQ_13_OBJ_ID)
	{
	    DAQ = true;
		// Read CAN message and clear interrupt
        uint8_t canRxDAQ13[CAN_RX_DAQ_13_LENGTH];
        can_read_message(CAN_RX_DAQ_13_OBJ_ID, canRxDAQ13);

		//
		// extract data and perform any scaling or shifting
		//
		CANParams.daq.torqueVectoring = (uint8_t)canRxDAQ13[0];
	}

	//-------------------------------------------------------------------------
	// check if DAQ 29 Message Received
	// CAN Message ID 0x329
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_DAQ_29_OBJ_ID)
	{
	    DAQ = true;
		// Read CAN message and clear interrupt
        uint8_t canRxDAQ29[CAN_RX_DAQ_29_LENGTH];
        can_read_message(CAN_RX_DAQ_29_OBJ_ID, canRxDAQ29);

		//
		// extract data and perform any scaling or shifting
		//
		uint8_t temp = (uint8_t)canRxDAQ29[0];
		CANParams.daq.lvInt = temp * CAN_SIG_DAQ_29_LV_INT;
		temp = temp - CANParams.daq.lvInt * 1/CAN_SIG_DAQ_29_LV_INT;
		CANParams.daq.lvDec = temp;
	}

/*
 * Motor Controller
 */
	//-------------------------------------------------------------------------
	// check if Motor Controller FL Message Received
	// CAN Message ID 0xC1
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_CONTROLLER_FL_OBJ_ID)
	{
	    Motor_Controller = true;
		// Read CAN message and clear interrupt
        uint8_t canRxControllerFL[CAN_RX_CONTROLLER_FL_LENGTH];
        can_read_message(CAN_RX_CONTROLLER_FL_OBJ_ID, canRxControllerFL);

		//
		// extract data and perform any scaling or shifting
		//
		CANParams.motorTemps.motorTempFL = (uint8_t)canRxControllerFL[6] * CAN_SIG_CONTROLLER_MOTOR_TEMP;
	}

	//-------------------------------------------------------------------------
	// check if Motor Controller FR Message Received
	// CAN Message ID 0xC2
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_CONTROLLER_FR_OBJ_ID)
	{
        Motor_Controller = true;
		// Read CAN message and clear interrupt
        uint8_t canRxControllerFR[CAN_RX_CONTROLLER_FR_LENGTH];
        can_read_message(CAN_RX_CONTROLLER_FR_OBJ_ID, canRxControllerFR);

		//
		// extract data and perform any scaling or shifting
		//
		CANParams.motorTemps.motorTempFR = (uint8_t)canRxControllerFR[6] * CAN_SIG_CONTROLLER_MOTOR_TEMP;
	}

	//-------------------------------------------------------------------------
	// check if Motor Controller RL Message Received
	// CAN Message ID 0xC3
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_CONTROLLER_RL_OBJ_ID)
	{
        Motor_Controller = true;
		// Read CAN message and clear interrupt
        uint8_t canRxControllerRL[CAN_RX_CONTROLLER_RL_LENGTH];
        can_read_message(CAN_RX_CONTROLLER_RL_OBJ_ID, canRxControllerRL);

		//
		// extract data and perform any scaling or shifting
		//
		CANParams.motorTemps.motorTempRL = (uint8_t)canRxControllerRL[6] * CAN_SIG_CONTROLLER_MOTOR_TEMP;
	}

	//-------------------------------------------------------------------------
	// check if Motor Controller RR Message Received
	// CAN Message ID 0xC4
	//-------------------------------------------------------------------------
	if (CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE) == CAN_RX_CONTROLLER_RR_OBJ_ID)
	{
        Motor_Controller = true;
		// Read CAN message and clear interrupt
        uint8_t canRxControllerRR[CAN_RX_CONTROLLER_RR_LENGTH];
        can_read_message(CAN_RX_CONTROLLER_RR_OBJ_ID, canRxControllerRR);

		//
		// extract data and perform any scaling or shifting
		//
		CANParams.motorTemps.motorTempRR = (uint8_t)canRxControllerRR[6] * CAN_SIG_CONTROLLER_MOTOR_TEMP;
	}

	//Timeout Check Function Call
	timeout(BMS, &lastBMSTime, &BMS_timeout);
    timeout(ECU, &lastECUTime, &ECU_timeout);
    timeout(SBG, &lastEKSTime, &EKS_timeout);
    timeout(DAQ, &lastDAQTime, &DAQ_timeout);
    timeout(Motor_Controller, &lastMotorTime, &Motor_timeout);
}

//*****************************************************************************
//
// Getters for CAN Signals
// Called in LCD.c to get the signal values to display on the screen
//
//*****************************************************************************

//-----------------------------------------------------------------------------
// Speed
//-----------------------------------------------------------------------------

/**
 * @return speed (mph)
 */
uint16_t GetSpeed(void)
{
	float speed;

	// compute speed using velocity_N and velocity_E
	speed = pow(CANParams.ekfVel.velocity_N, 2) + pow(CANParams.ekfVel.velocity_E, 2);
	speed = sqrt(speed);

	speed = (uint16_t)(speed * 0.00062137 * 60 * 60);

	return speed;
}

//-----------------------------------------------------------------------------
// Buttons
//-----------------------------------------------------------------------------

/**
 * @brief Send button states over CAN
 *
 * @param buttons current inputs
 * @param analog reading 0
 * @param analog reading 1
 */
void CANTxButtons(uint32_t buttons, uint16_t analog0, uint16_t analog1)
{
    uint8_t buttonData[8];
    buttonData[0] = (uint8_t)(buttons);
    buttonData[1] = (uint8_t)(buttons >> 8);
    buttonData[2] = (uint8_t)(buttons >> 16);
    buttonData[3] = (uint8_t)(buttons >> 24);

    buttonData[4] = (uint8_t)(analog0);
    buttonData[5] = (uint8_t)(analog0 >> 8);
    buttonData[6] = (uint8_t)(analog1);
    buttonData[7] = (uint8_t)(analog1 >> 8);

    can_send_message(CAN_TX_BUTTONS_OBJ_ID, buttonData);
}

/**
 * @brief returns true if no faults
 *
 * @author Mostafa Hassan
 * TODO: Other faults?
 */
bool noFaults(void)
{
    bool ECUMotorFaults1 = CANParams.ecuFlags.ECUMotorFlags1 == 0;
    bool ECUMotorFaults2 = CANParams.ecuFlags.ECUMotorFlags2 == 0;
    bool ECUOtherFaults = CANParams.ecuFlags.ECUOtherFlags == 0;

    return ECUMotorFaults1 && ECUMotorFaults2 && ECUOtherFaults;
}
