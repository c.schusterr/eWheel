/**
 * @brief Reading inputs.
 *
 * Initializes hardware to interface with the buttons. Functions to read the
 * button states.
 */

#include <stdint.h>
#include <stdbool.h>
#include "driverlib/adc.h"
#include "driverlib/can.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/ssi.h"
#include "inc/hw_adc.h"
#include "inc/hw_can.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inputs_ewheel.h"

//*****************************************************************************
//
// Global variables for this file
//
//*****************************************************************************
static uint16_t debounceCount[NUM_INPUTS];	//!< debounce count for each button

static uint32_t g_buttons;    //!< stores button states
static uint16_t g_analog0;    //!< stores analog0 state
static uint16_t g_analog1;    //!< stores analog1 state

/**
 * @brief setup up GPIO for digital inputs
 */
void DigitalInputInit(void)
{
	// Configure pins to be inputs
	GPIOPinTypeGPIOInput(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_3 | GPIO_PIN_6);
	GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_5 | GPIO_PIN_6);
	GPIOPinTypeGPIOInput(GPIO_PORTD_BASE, GPIO_PIN_2 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
	GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5);
	GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_4);

	// Enable PUR (pull up resistor) on pins to give default state
	GPIOPadConfigSet(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_3 | GPIO_PIN_6 | GPIO_PIN_7, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
	GPIOPadConfigSet(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_5 | GPIO_PIN_6, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
	GPIOPadConfigSet(GPIO_PORTD_BASE, GPIO_PIN_2 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
	GPIOPadConfigSet(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
	GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_4, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);

	// Initialize local variables for function
	int i;

	// Initialize the debounce count for all buttons to zero
	for (i = 0; i < NUM_INPUTS; i++)
	{
		debounceCount[i] = 0;
	}
}

/**
 * @brief initialize GPIO as analog inputs
 */
void AnalogInputInit(void)
{
        GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1);

        //enable clocking for ADC0
        SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);

        //wait for clocking
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0)) {};

        //Configure Sequence on ADC0
        ADCSequenceConfigure(ADC0_BASE, 0, ADC_TRIGGER_PROCESSOR, 0);

        // configure steps
        ADCSequenceStepConfigure(ADC0_BASE, 0, 0, ADC_CTL_CH7);
        ADCSequenceStepConfigure(ADC0_BASE, 0, 1, ADC_CTL_CH6 | ADC_CTL_END | ADC_CTL_IE);

        //Enable Interrupts
        ADCIntEnable(ADC0_BASE, 0);
        ADCIntClear(ADC0_BASE, 0);

        //Enable Peripheral
        ADCSequenceEnable(ADC0_BASE, 0);
}

/**
 * @brief reads the value of the analog inputs
 */
void AnalogInputRead(uint16_t * value0, uint16_t * value1)
{
    uint32_t buffer[8];

    //Trigger sequence
    ADCProcessorTrigger(ADC0_BASE, 0);

    //Wait for sequence to complete
    while(!ADCIntStatus(ADC0_BASE, 0, true));

    //Read values from ADC
    ADCSequenceDataGet(ADC0_BASE, 0, buffer);

    *value0 = buffer[0] & 0xfff;
    *value1 = buffer[1] & 0xfff;

    g_analog0 = buffer[0] & 0xfff;
    g_analog1 = buffer[1] & 0xfff;
}

/**
 * @brief Debounce buttons
 *
 * @returns Debounced button states
 */
void InputDebounce(void)
{
	// Initialize local variables for function
	uint8_t i = 0;
	uint32_t buttonsNew = 0;
	static uint32_t buttonsOutput = 0;

	// switches
	uint32_t PA6_tc_min = 0;
	uint32_t PA7_tc_mid = 0;
	uint32_t PB2_tc_max = 0;
	uint32_t PE1_button_4 = 0;
	uint32_t PA3_autox = 0;
	uint32_t PF1_endurance = 0;
	uint32_t PE3_skidpad = 0;
	uint32_t PA3_accel = 0;

	// buttons
	uint32_t PE2_button1 = 0;
	uint32_t PE5_button2 = 0;
	uint32_t PE4_button3 = 0;
	uint32_t PB1_button4 = 0;
	uint32_t PD5_button5 = 0;
	uint32_t PA0_button6 = 0;
	uint32_t PA1_button7 = 0;
	uint32_t PB3_button8 = 0;
	uint32_t PB5_button9 = 0;
	uint32_t PB6_button10 = 0;
	uint32_t PD7_button11 = 0;
	uint32_t PD6_button12 = 0;
	uint32_t PD4_button13 = 0;
	uint32_t PD2_button14 = 0;



	//
	// read in values and shift so the flag is in the 0th bit position
	//

	// switches
	PA6_tc_min = (GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_6) >> (6));
	PA7_tc_mid = (GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_7) >> (7));
	PB2_tc_max = (GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_2) >> (2));
	PE1_button_4 = (GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_1) >> (1));
	PD2_button14 = (GPIOPinRead(GPIO_PORTD_BASE, GPIO_PIN_2) >> 2);
	PD4_button13 = (GPIOPinRead(GPIO_PORTD_BASE, GPIO_PIN_4) >> 4);
	PD6_button12 = (GPIOPinRead(GPIO_PORTD_BASE, GPIO_PIN_6) >> 6);
	PD7_button11 = (GPIOPinRead(GPIO_PORTD_BASE, GPIO_PIN_7) >> 7);
	PB6_button10 = (GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_6) >> 6);
	PB5_button9 = (GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_5) >> 5);
	PB3_button8 = (GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_3) >> 3);
	PA1_button7 = (GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_1) >> 1);
	PA0_button6 = (GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_0) >> 0);
	PD5_button5 = (GPIOPinRead(GPIO_PORTD_BASE, GPIO_PIN_5) >> 5);
	PB1_button4 = (GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_1) >> (1));
	PE4_button3 = (GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_4) >> (4));
	PE5_button2 = (GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_5) >> (5));
	PE2_button1 = (GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_2) >> 2);

	// buttons
	PA3_accel = (GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_3) >> 3);
	PE3_skidpad = (GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_3) >> 3);
	PF1_endurance = (GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_1) >> 1);
	PA3_autox = (GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_0) >> 0);

	//
	// shift button values to position for CAN message tp be sent to ECU

	// switches
	PA6_tc_min <<= INPUT_TC_MIN;
	PA7_tc_mid <<= INPUT_TC_MID;
	PB2_tc_max <<= INPUT_TC_MAX;
	PE1_button_4 <<= BUTTON_4;
	PA3_autox <<= BUTTONS_AUTOCROSS_MODE;
	PF1_endurance <<= BUTTONS_ENDURANCE_MODE;
	PE3_skidpad <<= BUTTONS_SKIDPAD_MODE;
	PA3_accel <<= BUTTONS_ACCELERATION_MODE;

	// buttons
	PE2_button1 <<= BUTTONS_BUTTON_1;
	PE5_button2 <<= BUTTONS_BUTTON_2;
	PE4_button3 <<= BUTTONS_BUTTON_3;	// Change Screen Button
	PB1_button4 <<= BUTTONS_BUTTON_4;
	PD5_button5 <<= BUTTONS_BUTTON_5;
	PA0_button6 <<= BUTTONS_BUTTON_6;
	PA1_button7 <<= BUTTONS_BUTTON_7;
	PB3_button8 <<= BUTTONS_BUTTON_8;
	PB5_button9 <<= BUTTONS_BUTTON_9;
	PB6_button10 <<= BUTTONS_BUTTON_10;
	PD7_button11 <<= BUTTONS_BUTTON_11;
	PD6_button12 <<= BUTTONS_BUTTON_12;
	PD4_button13 <<= BUTTONS_BUTTON_13;
	PD2_button14 <<= BUTTONS_BUTTON_14;

	// combine each individual button flag into 1 variable
    buttonsNew = (PA6_tc_min | PA7_tc_mid | PB2_tc_max | PE1_button_4
            | PA3_autox | PF1_endurance | PE3_skidpad | PA3_accel | PE2_button1
            | PE5_button2 | PE4_button3 | PB1_button4 | PD5_button5
            | PA0_button6 | PA1_button7 | PB3_button8 | PB5_button9
            | PB6_button10 | PD7_button11 | PD6_button12 | PD4_button13
            | PD2_button14);

	// Perform button debouncing here
	// Cycle for loop through number of buttons we have on wheel
	// on a per button basis
	//
	// A 0 means the button is pressed. I.e. PURs are used to set the value high
	// when the button is not pressed.

	for(i = 0; i < NUM_INPUTS; i++)
	{
		// Check if button state is high
		if(!(buttonsNew & (1<<i)))
		{
			// If under max # of check, add to debounce count
			if(debounceCount[i] < MAX_INPUT_CHECKS)
			{
				debounceCount[i]++;
			}
			// If max debounce checks has been reached, set button output high
			else
			{
				buttonsOutput = buttonsOutput | (1<<i);
			}
		}

		// If button state isn't high, subtract debounce count down to zero
		// and output button off state
		else
		{
			if(debounceCount[i] > 0)
			{
				debounceCount[i]--;
			}
			else
			{
				buttonsOutput = buttonsOutput & ~(1 << i);
			}
		}
	}

	g_buttons = buttonsOutput;
}

uint32_t GetButtons(void)
{
    return g_buttons;
}

uint16_t GetAnalog0(void)
{
    return g_analog0;
}

uint16_t GetAnalog1(void)
{
    return g_analog1;
}
