/**
 * @brief Watchdog functions
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/watchdog.h"
#include "watchdog_ewheel.h"

/**
 * @brief Initialize the watchdog peripheral
 */
void WatchdogInitialize(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WDOG0);

    // Unlock watchdog: make changes
    if (WatchdogLockState(WATCHDOG0_BASE) == true)
    {
    	WatchdogUnlock(WATCHDOG0_BASE);
    }

    // Initialize the timer to half of the system clock
    // (so an interupt generates in 1/2 a second)
    WatchdogReloadSet( WATCHDOG0_BASE, SysCtlClockGet()/2 );

    // Allow watchdog to reset the chip
    WatchdogResetEnable(WATCHDOG0_BASE);

    // Enable watchdog timer (and interrupt)
    WatchdogEnable(WATCHDOG0_BASE);

    // lock the watchdog
    WatchdogLock(WATCHDOG0_BASE);
}

/**
 * @brief Watchdog interrupt handler
 */
void WatchdogIntHandler(void)
{
	//! TODO send CAN message
}
