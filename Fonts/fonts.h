/*
 * fonts.h
 *
 *  Created on: Jan 31, 2016
 *      Author: Mark
 */

#ifndef FONTS_H_
#define FONTS_H_

#include <stdbool.h>
#include <stdint.h>
#include "grlib/grlib.h"

/*
extern const tFont g_sFontCmss60;
#define g_pFontCmss60 (const tFont *)&g_sFontCmss60
extern const tFont g_sFontCmss70b;
#define g_pFontCmss70b (const tFont *)&g_sFontCmss70b
extern const tFont g_sFontCmss90b;
#define g_pFontCmss90b (const tFont *)&g_sFontCmss90b
extern const tFont g_sFontCmss96b;
#define g_pFontCmss96b (const tFont *)&g_sFontCmss96b
extern const tFont g_sFontCmss100b;
#define g_pFontCmss100b (const tFont *)&g_sFontCmss100b
extern const tFont g_sFontCmss100bi;
#define g_pFontCmss100bi (const tFont *)&g_sFontCmss100bi
extern const tFont g_sFontCmss100i;
#define g_pFontCmss100i (const tFont *)&g_sFontCmss100i
*/

//
// Font: Apercu
// Added by Kass Chupongstimun
//
extern const tFont g_sFontApercu20bi;
#define g_pFontApercu20bi (const tFont *)&g_sFontApercu20bi

extern const tFont g_sFontApercu12b;
#define g_pFontApercu12b (const tFont *)&g_sFontApercu12b

extern const tFont g_sFontApercu16b;
#define g_pFontApercu16b (const tFont *)&g_sFontApercu16b

extern const tFont g_sFontApercu18b;
#define g_pFontApercu18b (const tFont *)&g_sFontApercu18b

extern const tFont g_sFontApercu20b;
#define g_pFontApercu20b (const tFont *)&g_sFontApercu20b

extern const tFont g_sFontApercu22b;
#define g_pFontApercu22b (const tFont *)&g_sFontApercu22b

extern const tFont g_sFontApercu24b;
#define g_pFontApercu24b (const tFont *)&g_sFontApercu24b

extern const tFont g_sFontApercu26b;
#define g_pFontApercu26b (const tFont *)&g_sFontApercu26b

extern const tFont g_sFontApercu28b;
#define g_pFontApercu28b (const tFont *)&g_sFontApercu28b

extern const tFont g_sFontApercu32b;
#define g_pFontApercu32b (const tFont *)&g_sFontApercu32b

extern const tFont g_sFontApercu36b;
#define g_pFontApercu36b (const tFont *)&g_sFontApercu36b

extern const tFont g_sFontApercu42b;
#define g_pFontApercu42b (const tFont *)&g_sFontApercu42b

extern const tFont g_sFontApercu65b;
#define g_pFontApercu65b (const tFont *)&g_sFontApercu65b

extern const tFont g_sFontApercu100b;
#define g_pFontApercu100b (const tFont *)&g_sFontApercu100b


#endif /* FONTS_H_ */
