//*****************************************************************************
//
// This file is generated by ftrasterize; DO NOT EDIT BY HAND!
//
//*****************************************************************************
#include <stdbool.h>
#include <stdint.h>
#include "grlib/grlib.h"

//*****************************************************************************
//
// Details of this font:
//     Characters: 32 to 126 inclusive
//     Style: apercu
//     Size: 36 point
//     Bold: yes
//     Italic: no
//     Memory usage: 4208 bytes
//
//*****************************************************************************

//*****************************************************************************
//
// The compressed data for the 36 point Apercu bold font.
// Contains characters 32 to 126 inclusive.
//
//*****************************************************************************
static const unsigned char g_pucApercu36bData[4007] =
{
      5,  14,   0,  64,  96,  31,   9, 240, 197,  69,  69,  69,
     69,  69,  69,  69,  69,  69,  69,  69,  69,  69,  69,  69,
     69, 240, 240,  36,  70,  54,  54,  54,  68,   0,   9,  64,
     25,  15, 240, 240, 245,  37,  53,  37,  53,  36,  69,  36,
     68,  52,  83,  52,  83,  67,  83,  67,  83,  67,   0,  47,
     48,  59,  25,   0,  10,  51,  68, 227,  68, 227,  67, 228,
     67, 228,  67, 227,  68, 227,  68, 159,   5,  95,   5,  95,
      5,  95,   5, 147,  68, 227,  68, 227,  67, 228,  67, 159,
      5,  95,   5,  95,   5,  95,   5, 148,  67, 228,  67, 227,
     68, 227,  68, 227,  67, 228,  67, 228,  67,   0,  26,  48,
     44,  19, 101, 229, 229, 215, 170, 140, 124, 102,  38,  85,
     67, 117, 229, 230, 215, 216, 186, 170, 185, 185, 200, 214,
     82, 117,  52, 117,  53, 101,  53, 101,  54,  70,  78, 109,
    123, 167, 213, 229, 229,   0,  12, 112,  71,  30,   0,  10,
     18, 165, 211, 136, 164, 122, 133, 107, 117, 117,  37, 100,
    132,  68,  84, 148,  68,  69, 149,  37,  53, 172,  52, 202,
     52, 232,  53, 246,  53, 240, 164, 240, 164, 240, 165,  53,
    240,  21,  41, 228,  43, 196,  59, 181,  37,  53, 149,  52,
     84, 148,  68,  84, 132,  85,  53, 117,  92, 132, 123, 116,
    153, 131, 197, 178,   0,  29,  32,  55,  24,   0,   9, 102,
    240,  40, 250, 220, 197,  37, 196,  68, 196,  68, 196,  53,
    212,  21, 234, 248, 247, 240,  25, 234,  81, 117,  37,  50,
    102,  38,  35,  85,  74,  85,  89,  85, 104,  85, 118, 102,
    102, 118,  71, 127,   3, 127,   3, 123,  22, 135,  69,   0,
     24,  48,  15,   8, 240, 149,  53,  53,  53,  52,  83,  83,
     83,  83,   0,  25,  64,  35,  11, 240, 180, 101, 100, 101,
    100, 101, 101, 101, 100, 101, 101, 101, 101, 101, 101, 101,
    101, 101, 101, 101, 116, 117, 101, 101, 116, 117, 116, 117,
    116,   0,   8,  80,  35,  11, 240, 116, 117, 116, 117, 116,
    116, 117, 101, 116, 117, 101, 101, 101, 101, 101, 101, 101,
    101, 101, 101, 100, 101, 101, 101, 100, 101, 100, 101, 100,
      0,   9,  16,  26,  17,   0,   7,   4, 212, 212, 146,  36,
     34,  92,  78,  62, 104, 152, 152, 138, 116,  36, 130,  66,
    161,  65,   0,  43,  48,  39,  20,   0,  23,  52, 240,  20,
    240,  20, 240,  20, 240,  20, 240,  20, 159,   2,  63,   2,
     63,   2,  63,   2, 164, 240,  20, 240,  20, 240,  20, 240,
     20, 240,  20, 240,  20,   0,  28,  80,  18,  12,   0,  36,
     54,  86, 102, 101, 117, 101, 116, 132, 131, 132, 131, 240,
    240,  48,  11,  16,   0,  30,  13,  61,  61,  61,   0,  36,
     48,  13,   9,   0,  26,   4,  70,  54,  54,  54,  68,   0,
      9,  64,  66,  22, 240, 240, 116, 240,  37, 240,  36, 240,
     37, 240,  36, 240,  37, 240,  36, 240,  37, 240,  36, 240,
     37, 240,  36, 240,  36, 240,  52, 240,  36, 240,  52, 240,
     36, 240,  37, 240,  36, 240,  37, 240,  36, 240,  37, 240,
     36, 240,  37, 240,  36, 240,  37, 240,  36, 240,  36, 240,
     52, 240,  36, 240,  52,   0,  18,  96,  51,  22,   0,   9,
      7, 219, 173, 143, 118,  54, 102,  86,  85, 117,  85, 117,
     69, 149,  53, 149,  53, 149,  53, 149,  53, 149,  53, 149,
     53, 149,  53, 149,  53, 149,  53, 149,  69, 117,  85, 117,
     86,  86, 102,  54, 127, 141, 171, 215,   0,  23,  16,  35,
     13,   0,   5,  99, 133, 103,  88,  58,  58,  58,  51,  37,
     50,  53, 133, 133, 133, 133, 133, 133, 133, 133, 133, 133,
    133, 133, 133, 133, 133, 133, 133,   0,  13,  48,  38,  19,
      0,   7, 102, 186, 140, 110,  85,  69,  84, 101,  53, 101,
    229, 229, 229, 213, 214, 214, 198, 198, 213, 214, 198, 198,
    213, 213, 214, 207,  79,  63,   1,  63,   1,   0,  19,  48,
     37,  20,   0,   7,  94, 110, 109, 125, 229, 244, 245, 229,
    244, 244, 250, 186, 171, 230, 246, 245, 245, 245,  82, 133,
     52, 133,  53, 102,  69,  70,  95, 109, 139, 182,   0,  21,
     16,  50,  21,   0,   9,  22, 246, 231, 231, 216, 216, 201,
    186, 180,  21, 165,  21, 164,  37, 149,  37, 148,  53, 133,
     53, 117,  69, 116,  85, 101,  85, 111,   2,  63,   3,  63,
      3,  63,   3, 229, 240,  21, 240,  21, 240,  21, 240,  21,
      0,  21,  80,  40,  20,   0,   7, 109, 125, 125, 125, 116,
    240,  20, 245, 245, 244, 240,  20,  22, 157, 126, 111,  85,
     70,  83, 118, 245, 245, 245, 245,  53, 117,  53, 102,  69,
     70,  95, 109, 139, 183,   0,  21,  42,  20,   0,   8,  69,
    245, 229, 244, 245, 244, 245, 229, 245, 233, 187, 141, 126,
     86,  54,  85,  86,  53, 117,  53, 117,  53, 117,  53, 117,
     53, 117,  54,  86,  70,  54,  94, 125, 138, 213,   0,  21,
     16,  33,  18,   0,   6, 111,  63,  63,  62, 228, 213, 212,
    213, 212, 228, 213, 212, 213, 213, 212, 213, 212, 228, 213,
    212, 213, 212, 228, 213, 212, 213,   0,  19,  64,  46,  20,
      0,   8,  22, 201, 171, 156, 117,  69, 100, 100, 100, 100,
    100, 100, 100, 100, 116,  69, 124, 154, 156, 126, 102,  54,
     70,  86,  53, 117,  53, 117,  53, 117,  53, 117,  54,  86,
     70,  54,  95, 109, 139, 183,   0,  21,  41,  20,   0,   8,
     22, 202, 157, 110, 102,  54,  70,  86,  53, 117,  53, 117,
     53, 117,  53, 117,  54,  86,  70,  54,  95, 109, 140, 169,
    230, 229, 230, 229, 229, 245, 229, 245, 229, 245,   0,  21,
     48,  21,   9,   0,  11,  52,  70,  54,  54,  54,  68,   0,
      8,  68,  70,  54,  54,  54,  68,   0,   9,  64,  26,  11,
      0,  14,  20, 102,  86,  86,  86, 100,   0,  10,  54,  85,
    101,  85, 101, 100, 116, 100, 116, 115, 131, 240, 240, 176,
     27,  17,   0,  18,  81, 227, 197, 167, 137, 105, 121, 105,
    135, 165, 199, 169, 169, 153, 169, 167, 197, 227, 240,  17,
      0,  21,  80,  25,  19,   0,  28,  79,   1,  63,   1,  63,
      1,  63,   1,   0,   7,  79,   1,  63,   1,  63,   1,  63,
      1,   0,  33,  80,  27,  17,   0,  17,   1, 240,  19, 229,
    199, 169, 169, 153, 169, 167, 197, 167, 137, 105, 121, 105,
    135, 165, 195, 225,   0,  23,  32,  36,  17,   0,   6, 118,
    169, 123,  93,  53,  69,  52, 100, 212, 197, 166, 135, 150,
    165, 196,  49, 148,  51, 122, 122, 136, 180,   0,   5, 116,
    198, 182, 182, 182, 196,   0,  18,  90,  32,   0,  13,  57,
    240,  94, 240,  31,   3, 223,   5, 184, 104, 151, 167, 118,
    229, 117,  85,  20,  21,  86,  60,  21,  85,  60,  38,  69,
     45,  53,  53,  53,  53,  53,  53,  37,  84,  53,  53,  36,
    100,  53,  53,  36, 100,  53,  53,  36, 100,  53,  53,  36,
     85,  53,  53,  37,  54,  37,  69,  63,   5,  70,  47,   4,
    101,  56,  39, 118,  68,  84, 165, 240, 198, 240, 199, 193,
    216, 117, 223,   4, 239,   3, 240,  31, 240,  73,   0,  17,
     80,  57,  25,   0,  10,  69, 240,  70, 240,  70, 240,  71,
    240,  40, 240,  40, 240,  41, 250, 245,  20, 244,  37, 213,
     37, 213,  37, 212,  69, 181,  69, 181,  69, 181,  84, 175,
      1, 159,   1, 159,   1, 143,   3, 117, 133, 117, 148, 101,
    165,  85, 165,  85, 165,  69, 197,   0,  25,  48,  53,  21,
      0,   7, 125, 143, 111,   1,  95,   1,  85, 102,  69, 117,
     69, 117,  69, 117,  69, 117,  69, 101,  95,   1,  94, 127,
      1,  95,   2,  69, 102,  69, 118,  53, 133,  53, 133,  53,
    133,  53, 133,  53, 118,  53, 102,  79,   2,  79,   1,  95,
    109,   0,  22,  55,  26,   0,  10, 104, 240,  28, 223, 159,
      3, 135,  86, 118, 134,  86, 165,  85, 197,  69, 193, 117,
    240, 101, 240, 101, 240, 101, 240, 101, 240, 101, 240, 101,
    240, 101, 240, 117, 193, 133, 197,  70, 165, 102, 134, 119,
     86, 143,   3, 175, 204, 240,  24,   0,  27,  32,  55,  23,
      0,   8,  92, 190, 159,   1, 127,   2, 101,  88,  85, 118,
     85, 134,  69, 149,  69, 149,  69, 165,  53, 165,  53, 165,
     53, 165,  53, 165,  53, 165,  53, 165,  53, 165,  53, 149,
     69, 149,  69, 134,  69, 118,  85,  88,  95,   2, 111,   1,
    126, 156,   0,  24,  48,  41,  19,   0,   7,  31,   1,  63,
      1,  63,   1,  63,   1,  53, 229, 229, 229, 229, 229, 238,
     94,  94,  94,  85, 229, 229, 229, 229, 229, 229, 229, 239,
      1,  63,   1,  63,   1,  63,   1,   0,  19,  48,  37,  19,
      0,   7,  31,   1,  63,   1,  63,   1,  63,   1,  53, 229,
    229, 229, 229, 229, 229, 238,  94,  94,  94,  85, 229, 229,
    229, 229, 229, 229, 229, 229, 229, 229,   0,  20,  96,  57,
     25,   0,  10,  55, 240,  28, 206, 159,   2, 134,  87, 102,
    133,  86, 165,  69, 181,  69, 194,  85, 240,  85, 240,  85,
    240,  85, 240,  85, 137,  53, 137,  53, 137,  53, 137,  69,
    181,  69, 181,  70, 165,  86, 134,  87,  88, 111,   4, 127,
      3, 155,  20, 183,  52,   0,  25,  48,  59,  23,   0,   8,
     85, 165,  53, 165,  53, 165,  53, 165,  53, 165,  53, 165,
     53, 165,  53, 165,  53, 165,  53, 165,  63,   5,  63,   5,
     63,   5,  63,   5,  53, 165,  53, 165,  53, 165,  53, 165,
     53, 165,  53, 165,  53, 165,  53, 165,  53, 165,  53, 165,
     53, 165,  53, 165,   0,  23,  48,  32,   8, 240, 149,  53,
     53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,
     53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,
      0,   8,  48,  35,  19,   0,   8,  69, 229, 229, 229, 229,
    229, 229, 229, 229, 229, 229, 229, 229, 229, 229, 229, 229,
    229, 229,  68, 101,  53,  86,  54,  69,  94,  93, 138, 182,
      0,  20,  54,  23,   0,   8,  85, 134,  69, 118,  85, 102,
    101,  86, 117,  85, 133,  70, 133,  54, 149,  38, 165,  22,
    187, 202, 217, 234, 219, 197,  21, 197,  22, 181,  38, 165,
     54, 149,  69, 149,  70, 133,  86, 117, 102, 101, 117, 101,
    118,  85, 134,  69, 150,   0,  23,  48,  37,  19,   0,   7,
     21, 229, 229, 229, 229, 229, 229, 229, 229, 229, 229, 229,
    229, 229, 229, 229, 229, 229, 229, 229, 229, 229, 239,   1,
     63,   1,  63,   1,  63,   1,   0,  19,  48,  77,  26,   0,
      9, 102, 182,  54, 182,  54, 167,  55, 151,  55, 151,  56,
    120,  56, 120,  56, 120,  57,  89,  57,  89,  57,  89,  53,
     20,  52,  21,  53,  20,  52,  21,  53,  20,  37,  21,  53,
     36,  20,  37,  53,  36,  20,  37,  53,  41,  37,  53,  55,
     53,  53,  55,  53,  53,  55,  53,  53,  69,  69,  53,  69,
     69,  53,  69,  69,  53,  83,  85,  53, 213,  53, 213,   0,
     26,  48,  65,  22,   0,   8,  37, 149,  54, 133,  55, 117,
     55, 117,  56, 101,  56, 101,  57,  85,  57,  85,  58,  69,
     58,  69,  53,  21,  53,  53,  21,  53,  53,  37,  37,  53,
     37,  37,  53,  53,  21,  53,  53,  21,  53,  74,  53,  74,
     53,  89,  53,  89,  53, 104,  53, 104,  53, 119,  53, 119,
     53, 134,  53, 149,   0,  22,  48,  57,  27,   0,  11,  24,
    240,  44, 223,   1, 175,   3, 135,  87, 134, 134, 102, 166,
     85, 197,  85, 197,  69, 229,  53, 229,  53, 229,  53, 229,
     53, 229,  53, 229,  53, 229,  53, 229,  69, 197,  85, 197,
     86, 166, 102, 134, 119, 103, 143,   3, 175,   1, 220, 240,
     40,   0,  28,  48,  53,  21,   0,   7, 125, 143, 111,   1,
     95,   2,  69, 117,  69, 118,  53, 133,  53, 133,  53, 133,
     53, 133,  53, 118,  53, 117,  79,   2,  79,   1,  95, 109,
    133, 240,  21, 240,  21, 240,  21, 240,  21, 240,  21, 240,
     21, 240,  21, 240,  21, 240,  21,   0,  23,  61,  27,   0,
     11,  24, 240,  44, 223,   1, 175,   3, 135,  87, 134, 134,
    102, 166,  85, 197,  85, 197,  69, 229,  53, 229,  53, 229,
     53, 229,  53, 229,  53, 161,  53,  53, 147,  37,  53, 133,
     21,  69, 122,  85, 137,  86, 136, 102, 119, 119, 104, 127,
      6, 127,   5, 156,  35, 200,  81,   0,  27,  80,  53,  22,
      0,   8,  44, 174, 143, 127,   1, 101, 102,  85, 117,  85,
    117,  85, 117,  85, 117,  85, 117,  85, 101, 111,   1, 111,
    126, 140, 165,  53, 149,  53, 149,  69, 133,  69, 133,  85,
    117, 101, 101, 101, 101, 117,  85, 118,  69, 133,  69, 149,
      0,  22,  48,  39,  19,   0,   7, 102, 186, 140, 124, 102,
     38,  85,  67, 117, 229, 230, 215, 216, 186, 170, 185, 185,
    200, 214,  82, 117,  52, 117,  53, 101,  53, 101,  54,  69,
     94, 108, 138, 182,   0,  20,  58,  22,   0,   8,  47,   4,
     63,   4,  63,   4,  63,   4, 165, 240,  37, 240,  37, 240,
     37, 240,  37, 240,  37, 240,  37, 240,  37, 240,  37, 240,
     37, 240,  37, 240,  37, 240,  37, 240,  37, 240,  37, 240,
     37, 240,  37, 240,  37, 240,  37, 240,  37, 240,  37, 240,
     37,   0,  23,  32,  55,  22,   0,   8,  37, 149,  53, 149,
     53, 149,  53, 149,  53, 149,  53, 149,  53, 149,  53, 149,
     53, 149,  53, 149,  53, 149,  53, 149,  53, 149,  53, 149,
     53, 149,  53, 149,  53, 149,  53, 149,  53, 149,  53, 134,
     69, 117,  86,  86, 111, 141, 171, 215,   0,  23,  16,  57,
     25,   0,   9,  53, 182,  69, 165,  85, 165,  85, 150, 101,
    133, 117, 133, 117, 133, 117, 117, 149, 101, 149, 101, 149,
     85, 181,  69, 181,  69, 181,  68, 212,  53, 213,  37, 213,
     36, 244,  21, 250, 249, 240,  40, 240,  40, 240,  39, 240,
     70, 240,  70, 240,  69,   0,  26,  64,  84,  34,   0,  12,
    101, 240, 101,  53, 240, 101,  53, 240, 101,  68, 133, 133,
     69, 117, 117,  85, 117, 117,  85, 103, 101, 100, 103, 101,
    100, 103, 100, 117,  88,  69, 117,  73,  69, 132,  68,  20,
     69, 132,  68,  20,  68, 149,  37,  21,  52, 149,  36,  52,
     37, 164,  36,  52,  37, 164,  36,  53,  20, 180,  21,  68,
     20, 185,  89, 200,  89, 200,  88, 215, 119, 215, 119, 230,
    119, 229, 149, 245, 149,   0,  35,  16,  53,  23,   0,   8,
    101, 118,  85, 117, 117,  85, 134,  69, 149,  53, 181,  37,
    181,  21, 218, 217, 248, 247, 240,  37, 240,  54, 240,  23,
    240,  24, 233, 234, 197,  21, 182,  37, 165,  53, 150,  69,
    133,  86, 101, 117, 101, 118,  69, 149,  69, 165,   0,  23,
     48,  55,  22,   0,   8,  37, 149,  53, 149,  69, 117,  85,
    117, 101,  85, 117,  85, 133,  53, 149,  53, 165,  21, 181,
     21, 201, 217, 231, 247, 240,  21, 240,  37, 240,  37, 240,
     37, 240,  37, 240,  37, 240,  37, 240,  37, 240,  37, 240,
     37, 240,  37, 240,  37,   0,  23,  32,  46,  21,   0,   7,
    127,   2,  79,   2,  79,   2,  79,   1, 246, 245, 246, 245,
    245, 240,  21, 245, 240,  21, 245, 246, 245, 246, 245, 245,
    240,  21, 245, 240,  21, 245, 240,  31,   2,  63,   3,  63,
      3,  63,   3,   0,  21,  48,  35,  12, 201,  57,  57,  57,
     53, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117,
    117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 121,  57,
     57,  57,   0,   9,  48,  65,  23, 240, 133, 240,  68, 240,
     84, 240,  68, 240,  84, 240,  68, 240,  84, 240,  69, 240,
     68, 240,  69, 240,  68, 240,  69, 240,  68, 240,  69, 240,
     68, 240,  69, 240,  68, 240,  84, 240,  68, 240,  84, 240,
     68, 240,  84, 240,  69, 240,  68, 240,  69, 240,  68, 240,
     69, 240,  68, 240,  69, 240,  68,   0,  17,  80,  35,  12,
    201,  57,  57,  57, 117, 117, 117, 117, 117, 117, 117, 117,
    117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117,
    117, 117,  57,  57,  57,  57,   0,   9,  48,  25,  20,   0,
      8,  52, 246, 215, 216, 180,  21, 149,  36, 148,  53, 117,
     68, 101, 100,  84, 117,  53, 132,   0,  57, 112,  15,  19,
      0,  68, 127,   1,  63,   1,  63,   1,  63,   1,   0,   9,
    112,  12,  11, 240, 240,  54, 101, 117, 116, 132,   0,  40,
     32,  34,  18,   0,  23,  22, 170, 124,  93,  85,  54,  98,
     85, 213, 123,  93,  78,  78,  54,  69,  53,  85,  53,  70,
     53,  55,  63,  78,  72,  36, 101,  52,   0,  18,  48,  56,
     20,   0,   7,  68, 240,  20, 240,  20, 240,  20, 240,  20,
    240,  20, 240,  20, 240,  20,  53, 132,  25, 111,  95,   1,
     70,  70,  69, 101,  69, 102,  52, 133,  52, 133,  52, 133,
     52, 133,  52, 133,  53, 102,  53, 101,  70,  70,  79,   1,
     79,  84,  25, 100,  38,   0,  21,  31,  20,   0,  25, 102,
    202, 157, 110,  86,  70,  69, 101,  54, 114,  85, 245, 245,
    245, 245, 246, 114, 101, 101,  70,  70,  94, 125, 139, 182,
      0,  21,  56,  20,   0,   9,  20, 240,  20, 240,  20, 240,
     20, 240,  20, 240,  20, 240,  20, 133,  52, 105,  20,  95,
     79,   1,  70,  70,  69, 101,  54, 101,  53, 132,  53, 132,
     53, 132,  53, 132,  53, 132,  54, 101,  69, 101,  70,  70,
     79,   1,  95, 105,  20, 134,  36,   0,  20,  48,  35,  20,
      0,  25, 102, 201, 157, 125, 102,  69,  84, 116,  69, 117,
     63,   2,  63,   2,  63,   2,  63,   2,  53, 245, 129, 117,
    101,  70,  69, 110, 109, 154, 198,   0,  21,  32,  15,   0,
      6,  54, 120, 105,  90,  86, 149, 165, 139,  75,  75,  75,
    101, 165, 165, 165, 165, 165, 165, 165, 165, 165, 165, 165,
    165, 165, 165,   0,  16,  46,  20,   0,  24,  33, 181,  20,
    141, 110, 107, 133,  53, 116,  84, 116,  84, 116,  84, 117,
     53, 139, 155, 139, 138, 164, 240,  20, 240,  28, 157, 126,
     95,   1,  52, 118,  52, 133,  52, 133,  53, 102,  79,  94,
    124, 183, 128,  52,  19,   0,   7,  21, 229, 229, 229, 229,
    229, 229, 229,  53, 101,  25,  69,  25,  79,   1,  55,  54,
     54,  85,  54,  85,  53, 101,  53, 101,  53, 101,  53, 101,
     53, 101,  53, 101,  53, 101,  53, 101,  53, 101,  53, 101,
     53, 101,  53, 101,   0,  19,  48,  32,   9, 240,  68,  70,
     54,  54,  54,  68, 240, 117,  69,  69,  69,  69,  69,  69,
     69,  69,  69,  69,  69,  69,  69,  69,  69,  69,  69,  69,
      0,   9,  64,  40,  13, 240, 240,  20, 134, 118, 118, 118,
    132, 240, 240,  69, 133, 133, 133, 133, 133, 133, 133, 133,
    133, 133, 133, 133, 133, 133, 133, 133, 133, 133, 133, 118,
    102,  88, 102, 117, 131, 161, 176,  48,  20,   0,   7,  69,
    245, 245, 245, 245, 245, 245, 245,  86,  69,  85,  85,  69,
    101,  54, 101,  38, 117,  37, 133,  21, 154, 169, 186, 170,
    165,  21, 149,  37, 133,  38, 117,  53, 117,  69, 101,  85,
     85,  86,  69, 102,   0,  20,  48,  32,   8, 240, 149,  53,
     53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,
     53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,
      0,   8,  48,  63,  28,   0,  35,   5,  52, 100, 101,  23,
     56,  69,  24,  25,  79,  10,  55,  40,  38,  54,  70,  69,
     54,  70,  69,  53,  85,  85,  53,  85,  85,  53,  85,  85,
     53,  85,  85,  53,  85,  85,  53,  85,  85,  53,  85,  85,
     53,  85,  85,  53,  85,  85,  53,  85,  85,  53,  85,  85,
     53,  85,  85,   0,  28,  48,  45,  19,   0,  23, 101,  53,
    101,  40,  69,  25,  79,   1,  55,  54,  54,  85,  54,  85,
     53, 101,  53, 101,  53, 101,  53, 101,  53, 101,  53, 101,
     53, 101,  53, 101,  53, 101,  53, 101,  53, 101,  53, 101,
      0,  19,  48,  37,  22,   0,  28,  39, 219, 173, 143, 102,
     71,  85, 117,  70, 118,  53, 149,  53, 149,  53, 149,  53,
    149,  53, 149,  54, 118,  69, 117,  86,  86, 111, 141, 171,
    215,   0,  23,  16,  57,  20,   0,  25,   4,  53, 132,  25,
    111,  95,   1,  70,  70,  69, 101,  69, 102,  52, 133,  52,
    133,  52, 133,  52, 133,  52, 133,  53, 102,  53, 101,  70,
     70,  79,   1,  79,  84,  25, 100,  53, 132, 240,  20, 240,
     20, 240,  20, 240,  20, 240,  20, 240,  20, 240,  20, 240,
     16,  57,  20,   0,  25,  85,  52, 105,  20,  95,  79,   1,
     70,  70,  69, 101,  54, 101,  53, 132,  53, 132,  53, 132,
     53, 132,  53, 132,  54, 101,  69, 101,  70,  70,  79,   1,
     95, 105,  20, 133,  52, 240,  20, 240,  20, 240,  20, 240,
     20, 240,  20, 240,  20, 240,  20, 240,  20,  48,  28,  15,
      0,  18, 101,  52,  53,  22,  60,  60,  56, 119, 134, 150,
    149, 165, 165, 165, 165, 165, 165, 165, 165, 165, 165,   0,
     16,  32,  30,  16,   0,  20,  69, 168, 122,  91,  84,  51,
    100, 196, 198, 184, 152, 167, 197,  81, 100,  67,  84,  53,
     53,  75,  91, 105, 149,   0,  16, 112,  33,  15,   0,   6,
     50, 180, 165, 165, 165, 165, 165, 124,  60,  60,  60, 101,
    165, 165, 165, 165, 165, 165, 165, 165, 165, 166, 168, 120,
    135, 165,   0,  15,  48,  45,  19,   0,  23, 101, 101,  53,
    101,  53, 101,  53, 101,  53, 101,  53, 101,  53, 101,  53,
    101,  53, 101,  53, 101,  53, 101,  53,  86,  53,  86,  53,
     86,  54,  55,  63,   1,  73,  21,  72,  37, 101,  53,   0,
     19,  48,  38,  19,   0,  23, 101, 116,  68, 116,  68, 101,
     69,  84, 100,  84, 100,  69, 100,  68, 132,  52, 132,  52,
    132,  36, 164,  20, 164,  20, 164,  20, 183, 199, 199, 213,
    229, 229,   0,  20,  60,  29,   0,  36,  37, 100, 101,  53,
     85, 100,  84,  85, 100,  84,  86,  69,  85,  70,  68, 116,
     55,  68, 116,  55,  68, 116,  56,  36, 133,  35,  20,  36,
    148,  20,  20,  36, 148,  20,  35,  35, 164,  20,  40, 183,
     56, 183,  71, 183,  70, 214,  70, 213,  86, 213, 100, 244,
    100,   0,  30,  16,  37,  19,   0,  23, 101,  85,  85,  68,
    116,  53, 117,  36, 148,  21, 153, 183, 199, 213, 229, 215,
    200, 169, 164,  21, 133,  36, 117,  53, 100,  85,  69,  85,
     68, 117,   0,  19,  48,  46,  20,   0,  25,   5, 132,  68,
    117,  68, 116,  85, 100, 100,  85, 101,  68, 117,  68, 132,
     53, 133,  36, 164,  36, 164,  21, 169, 200, 200, 199, 230,
    230, 244, 240,  20, 245, 244, 245, 244, 240,  20, 245, 244,
    240,  20, 192,  26,  17,   0,  21,  45,  77,  77,  76, 196,
    197, 196, 197, 196, 196, 212, 196, 212, 196, 196, 221,  62,
     62,  62,   0,  17,  48,  36,  13, 240,  53, 118, 103, 103,
    101, 133, 133, 133, 133, 133, 133, 133, 118,  88,  87, 103,
    104, 118, 133, 133, 133, 133, 133, 133, 133, 133, 135, 103,
    118, 133,   0,  10,  16,  35,   8, 240,  21,  53,  53,  53,
     53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,
     53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,  53,
     53,   0,   6,  48,  35,  13, 214, 119, 104,  88, 133, 133,
    133, 133, 133, 133, 133, 133, 134, 119, 118, 118, 103, 102,
    117, 133, 133, 133, 133, 133, 133, 133,  88,  88,  87, 102,
      0,  10,  80,  20,  21,   0,  34,  69, 113, 120,  67,  95,
      2,  63,   3,  63,   2,  82,  73, 245,   0,  45,  48,
};

//*****************************************************************************
//
// The font definition for the 36 point Apercu bold font.
//
//*****************************************************************************
const tFont g_sFontApercu36b =
{
    //
    // The format of the font.
    //
    FONT_FMT_PIXEL_RLE,

    //
    // The maximum width of the font.
    //
    30,

    //
    // The height of the font.
    //
    37,

    //
    // The baseline of the font.
    //
    29,

    //
    // The offset to each character in the font.
    //
    {
           0,    5,   36,   61,  120,  164,  235,  290,
         305,  340,  375,  401,  440,  458,  469,  482,
         548,  599,  634,  672,  709,  759,  799,  841,
         874,  920,  961,  982, 1008, 1035, 1060, 1087,
        1123, 1213, 1270, 1323, 1378, 1433, 1474, 1511,
        1568, 1627, 1659, 1694, 1748, 1785, 1862, 1927,
        1984, 2037, 2098, 2151, 2190, 2248, 2303, 2360,
        2444, 2497, 2552, 2598, 2633, 2698, 2733, 2758,
        2773, 2785, 2819, 2875, 2906, 2962, 2997, 3029,
        3075, 3127, 3159, 3199, 3247, 3279, 3342, 3387,
        3424, 3481, 3538, 3566, 3596, 3629, 3674, 3712,
        3772, 3809, 3855, 3881, 3917, 3952, 3987,
    },

    //
    // A pointer to the actual font data
    //
    g_pucApercu36bData
};
